module src {

    export class Main {

        protected readonly app:PIXI.Application;

        constructor() {

            //creation and configuration the application
            this.app = new PIXI.Application(window.innerWidth, window.innerHeight);
            this.app.renderer.backgroundColor = 0xAAFFF6;

            let stageScale:number = Math.min(
                    window.innerWidth / (Config.MAP_TILES_HORIZONTAL_NUM * Config.TILE_SIZE_PX),
                        window.innerHeight / (Config.MAP_TILES_VERTICAL_NUM * Config.TILE_SIZE_PX)
                    );
            this.app.stage.scale.set(stageScale, stageScale);

            document.body.appendChild(this.app.view);

            this.app.view.style.marginTop = ( window.innerHeight -
                (Config.MAP_TILES_HORIZONTAL_NUM * Config.TILE_SIZE_PX) * stageScale) / 2 + 'px';
            this.app.view.style.marginLeft = (window.innerWidth -
                (Config.MAP_TILES_VERTICAL_NUM * Config.TILE_SIZE_PX) * stageScale) / 2 + 'px';


            //start game logic
            new factory.gamestate.GameStatesFactory(this.app)
                .createPreloadController()
                .startPreload();


        }
    }
}
let game:src.Main;
window.onload = ()=>{
    game = new src.Main();
}