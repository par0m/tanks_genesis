///<reference path="../../model/object/TileModel.ts"/>
module src.factory.object {
    import TileModel = src.model.object.TileModel;
    import AbstractGameObjectModel = src.model.object.AbstractGameObjectModel;
    import AbstractTankModel = src.model.object.tank.TankModel;
    import AbstractBlockModel = src.model.object.block.AbstractBlockModel;
    import TankModel = src.model.object.tank.TankModel;
    import Direction = src.model.object.Direction;

    export abstract class AbstractTilesMatrixFactory {

        readonly matrix:Array<Array<TileModel>> = [];
        readonly tankModels:Array<TankModel> = [];

        /**
         * Creates free matrix
         * @returns {Array<Array<src.model.object.TileModel>>}
         */
        constructor() {
            for (let i:number = 0; i < Config.MAP_TILES_HORIZONTAL_NUM; i++) {
                let array:Array<TileModel> = [];
                for (let j:number = 0; j < Config.MAP_TILES_VERTICAL_NUM; j++) {
                    let tile:TileModel = new TileModel(i, j, Config.TILE_SIZE_PX);
                    array.push(tile);
                }
                this.matrix.push(array);
            }

            //setup neighborsAsVector
            for (let i:number = 0; i < Config.MAP_TILES_HORIZONTAL_NUM; i++) {
                for (let j:number = 0; j < Config.MAP_TILES_VERTICAL_NUM; j++) {
                    let tile:TileModel = this.matrix[i][j];
                    let neighborsAsVector:Array<TileModel> = [];
                    let neighborsAsDirectionMap:Object = new Object();

                    for (let k:number = -1; k <= 1; k++) {
                        for (let l:number = -1; l <= 1; l++) {
                            if (k == 0 && l == 0) continue;

                            let _i:number = i + k;
                            let _j:number = j + l;
                            if (_i >= 0 && _i < Config.MAP_TILES_HORIZONTAL_NUM &&
                                _j >= 0 && _j < Config.MAP_TILES_VERTICAL_NUM) {
                                neighborsAsVector.push(this.matrix[_i][_j]);

                                let _direction:Direction = null;
                                if (k == 1 && l == 0) {
                                    _direction = Direction.RIGHT;
                                } else if (k == 0 && l == 1) {
                                    _direction = Direction.DOWN;
                                } else if (k == -1 && l == 0) {
                                    _direction = Direction.LEFT;
                                } else if (k == 0 && l == -1) {
                                    _direction = Direction.UP;
                                }
                                if (_direction != null) {
                                    neighborsAsDirectionMap[_direction] = this.matrix[_i][_j];
                                }
                            }
                        }
                    }

                    tile.neighborsAsVector = neighborsAsVector;
                    tile.neighborsAsDirectionMap = neighborsAsDirectionMap;
                }
            }
        }

        protected abstract generateTankModels():void;

        protected abstract generateBlockObjectModels(count:number, hayBlockModelProbability:number):Array<AbstractBlockModel>;

        protected abstract fill(freeTiles:Array<TileModel>, gameObjectModels:Array<AbstractGameObjectModel>):void

        protected getFreeMatrixTilesAsArray():Array<TileModel> {
            let freeTiles:Array<TileModel> = [];
            for (let i:number = 0; i < this.matrix.length; i++) {
                for (let j:number = 0; j < this.matrix[i].length; j++) {
                    if (this.matrix[i][j].gameObject == null) {
                        freeTiles.push(this.matrix[i][j]);
                    }
                }
            }
            return freeTiles;
        }

    }
}