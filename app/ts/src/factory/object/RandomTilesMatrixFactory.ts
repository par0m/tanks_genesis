///<reference path="../../model/object/TileModel.ts"/>
///<reference path="../../model/object/block/HayBlockModel.ts"/>
///<reference path="../../model/object/block/WallBlockModel.ts"/>
///<reference path="../../model/object/tank/RedTankModel.ts"/>
///<reference path="../../model/object/tank/GreenTankModel.ts"/>
///<reference path="../../model/object/tank/BlueTankModel.ts"/>
///<reference path="../../util/CommonUtils.ts"/>
///<reference path="AbstractTilesMatrixFactory.ts"/>
module src.factory.object {
    import TileModel = src.model.object.TileModel;
    import HayBlockModel = src.model.object.block.HayBlockModel;
    import WallBlockModel = src.model.object.block.WallBlockModel;
    import AbstractGameObjectModel = src.model.object.AbstractGameObjectModel;
    import AbstractBlockModel = src.model.object.block.AbstractBlockModel;
    import AbstractTankModel = src.model.object.tank.TankModel;
    import RedTankModel = src.model.object.tank.RedTankModel;
    import GreenTankModel = src.model.object.tank.GreenTankModel;
    import BlueTankModel = src.model.object.tank.BlueTankModel;
    import CommonUtils = src.util.CommonUtils;

    export class RandomTilesMatrixFactory extends AbstractTilesMatrixFactory {

        protected matrixFillingByBlocks:number = 0.4;
        protected hayBlockModelProbability:number = 0.75;

        constructor() {
            super();

            let freeTiles:Array<TileModel> = this.getFreeMatrixTilesAsArray();

            this.generateTankModels();
            this.fill(freeTiles, this.tankModels);

            let blocksCount:number = Math.floor(freeTiles.length * this.matrixFillingByBlocks);
            let blocksModels:Array<AbstractBlockModel> = this.generateBlockObjectModels(blocksCount, this.hayBlockModelProbability);
            this.fill(freeTiles, blocksModels);
        }


        /**
         * Generates three different tanks
         * @returns {Array<src.model.object.tank.AbstractTankModel>}
         */
        protected generateTankModels():void {
            this.tankModels.push(new RedTankModel(), new GreenTankModel(), new BlueTankModel());
        }

        protected generateBlockObjectModels(count:number, hayBlockModelProbability:number):Array<AbstractBlockModel> {
            let rndArray:Array<AbstractBlockModel> = new Array(count);
            for (let i:number = 0; i < count; i++) {
                rndArray[i] = Math.random() < hayBlockModelProbability ? new HayBlockModel() : new WallBlockModel();
            }
            return rndArray;
        }

        protected fill(freeTiles:Array<TileModel>, gameObjectModels:Array<AbstractGameObjectModel>):void {
            for (let i:number = 0; i < gameObjectModels.length && freeTiles.length > 0; i++) {
                let rndFreeTileModel:TileModel = CommonUtils.removeRandomItem(freeTiles, 0);
                gameObjectModels[i].tile = rndFreeTileModel;
                gameObjectModels[i].position.set(rndFreeTileModel.position.x, rndFreeTileModel.position.y);
            }
        }
    }
}