///<reference path="../object/RandomTilesMatrixFactory.ts"/>
///<reference path="../../viewmodel/object/GameObjectViewModel.ts"/>
module src.factory.gamestate {
    import RandomTilesMatrixFactory = src.factory.object.RandomTilesMatrixFactory;
    import BattleAreaModel = src.model.gamestate.BattleAreaModel;
    import GameObjectView = src.view.object.GameObjectView;
    import GameObjectViewModel = src.viewmodel.object.GameObjectViewModel;

    export class GameStatesFactory {
        public static instance:GameStatesFactory;

        protected app:PIXI.Application;

        constructor(app:PIXI.Application) {
            this.app = app;
            GameStatesFactory.instance = this;
        }

        //***************************************** Preload MVC ******************************************/
        private createPreloadView():view.gamestate.PreloadView {
            return new view.gamestate.PreloadView(this.app);
        }
        public createPreloadController():controler.gamestate.PreloadController {
            return new controler.gamestate.PreloadController(GameStatesFactory.instance.createPreloadView());
        }
        //***************************************** BattleAre MVC ******************************************/
        private createBattleAreaModel():model.gamestate.BattleAreaModel {
            let rndtmf:RandomTilesMatrixFactory = new RandomTilesMatrixFactory();
            return new model.gamestate.BattleAreaModel(rndtmf.matrix, rndtmf.tankModels);
        }
        public createBattleAreaController():controler.gamestate.BattleAreaController {
            let battleAreModel:BattleAreaModel = GameStatesFactory.instance.createBattleAreaModel();
            let gameObjectViewModelsArray:Array<GameObjectViewModel> = [];
            for (let row of battleAreModel.tilesMatrix) {
                for (let tile of row) {
                    if (tile.gameObject) {
                        let gameObjectView:GameObjectView = GameObjectViewModel.retrieveGameObjectView(tile.gameObject);
                        let gameObjectViewModel:GameObjectViewModel = GameObjectViewModel.bind(gameObjectView, tile.gameObject);
                        gameObjectViewModelsArray.push(gameObjectViewModel);
                    }
                }
            }
            return new controler.gamestate.BattleAreaController(
                battleAreModel,
                GameStatesFactory.instance.createBattleAreaView(),
                gameObjectViewModelsArray,
                this.app
            );
        }
        private createBattleAreaView():view.gamestate.BattleAreaView {
            return new view.gamestate.BattleAreaView(this.app);
        }





    }
}