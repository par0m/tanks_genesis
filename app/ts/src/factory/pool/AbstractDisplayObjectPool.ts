module src.factory.pool {
    import DisplayObject = PIXI.DisplayObject;
    import InteractionEventTypes = PIXI.interaction.InteractionEventTypes;

    /**
     * Pulling based on existing in PIXI display list
     * @author Roman
     */
    export abstract class AbstractDisplayObjectPool<T extends DisplayObject> {


        protected static readonly ADDED:InteractionEventTypes = 'added';
        protected static readonly REMOVED:InteractionEventTypes = 'removed';

        protected elements:Array<T>;
        protected freeElements:Array<T>;

        constructor() {
            this.elements = [];
            this.freeElements = [];
        }

        protected moveToElements(target:T = null):void {
            target.off(AbstractDisplayObjectPool.ADDED);
            this.move(target, this.freeElements, this.elements);
            target.on(AbstractDisplayObjectPool.REMOVED, () => {
                this.moveToFreeElements(target);
            }, this);
        }

        protected moveToFreeElements(target:T):void {
            target.off(AbstractDisplayObjectPool.REMOVED);
            this.move(target, this.elements, this.freeElements);
            target.on(AbstractDisplayObjectPool.ADDED, () => {
                this.moveToElements(target);
            }, this);
        }

        protected move(element:T, fromArray:Array<T>, toArray:Array<T>):void {
            let index:number;
            if ((index = fromArray.indexOf(element)) > -1) {
                fromArray.splice(index, 1);
            }
            if ((index = toArray.indexOf(element)) == -1) {
                toArray.push(element);
            }
        }

        public destroy(everyElementCallback:Function = null):void {
            for (let i:number = this.elements.length - 1; i > -1; i--) {
                this.elements[i].parent.removeChild(this.elements[i]);
            }
            if (everyElementCallback) {
                for (let j:number = 0; j < this.freeElements.length; j++) {
                    everyElementCallback(this.freeElements[j]);
                }
            }
            this.freeElements.splice(0, this.freeElements.length);
        }

    }

}