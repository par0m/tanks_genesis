///<reference path="AbstractDisplayObjectPool.ts"/>
module src.factory.pool {
    import GameObjectView = src.view.object.GameObjectView;

    export class GameObjectViewPool extends AbstractDisplayObjectPool<GameObjectView> {

        private static _instance:GameObjectViewPool = null;
        public static get instance():GameObjectViewPool {
            return GameObjectViewPool._instance ? GameObjectViewPool._instance
                : GameObjectViewPool._instance = new GameObjectViewPool();
        }

        constructor() {
            super();
        }

        /**
         *
         * @param {string} prefix will be used to create Texture and Sprite. See new view.object.GameObjectView(prefix)
         * @param {boolean} setAsUsing if true retrieved object will be used even if it is not added in PIXI display list
         * @returns {src.view.object.GameObjectView} retrieved object
         */

        public getElement(prefix:string, setAsUsed:boolean = false):GameObjectView {
            let object:GameObjectView = null;

            for (let i:number = 0; i < this.freeElements.length; i++) {
                if (this.freeElements[i].prefix == prefix) {
                    object = this.freeElements[i];
                    break;
                }
            }
            if (!object) {
                object = new view.object.GameObjectView(prefix);

                this.freeElements.push(object);
                object.on(AbstractDisplayObjectPool.ADDED, () => {
                    this.moveToElements(object);
                }, this);
            }
            if (setAsUsed) {
                this.moveToElements(object);
                object.off(AbstractDisplayObjectPool.ADDED);
            }

            return object;
        }
    }
}