///<reference path="../../util/CommonUtils.ts"/>
module src.model.object {
    import CommonUtils = src.util.CommonUtils;

    export abstract class MovableGameObject extends AbstractGameObjectModel {


        private _direction: Direction;
        private _isMoving: boolean = false;
        private _axleSpeedModule: number;
        protected directionSpeedsValueMap: Object = new Object();

        public get isMoving(): boolean {
            return this._isMoving;
        }

        public set isMoving(_isMoving: boolean) {
            this._isMoving = _isMoving;
        }

        public set axleSpeedModule(_axleSpeedModule: number) {
            this._axleSpeedModule = _axleSpeedModule;

            this.directionSpeedsValueMap[Direction.RIGHT] = {x: this._axleSpeedModule, y: 0};
            this.directionSpeedsValueMap[Direction.LEFT] = {x: -this._axleSpeedModule, y: 0};
            this.directionSpeedsValueMap[Direction.UP] = {x: 0, y: -this._axleSpeedModule};
            this.directionSpeedsValueMap[Direction.DOWN] = {x: 0, y: this._axleSpeedModule};
        }


        constructor(axleSpeedModule: number, direction: Direction) {
            super();
            this.axleSpeedModule = axleSpeedModule;
            this.direction = direction;
        }

        public get direction(): Direction {
            return this._direction;
        }

        public set direction(_direction: Direction) {
            this._direction = _direction;
        }

        public hitTestWith(otherAbstractGameObjectModel: AbstractGameObjectModel, paddingPercentage: number = 0.04): boolean {
            return (Math.abs(this.position.x - otherAbstractGameObjectModel.position.x)
                < (this.viewBounds.width + otherAbstractGameObjectModel.viewBounds.width) * (0.5 - paddingPercentage)
            ) && (Math.abs(this.position.y - otherAbstractGameObjectModel.position.y)
                < (this.viewBounds.height + otherAbstractGameObjectModel.viewBounds.height) * (0.5 - paddingPercentage));
        }

        /**
         * Finds the closest free tile to this GameObject
         */
        protected findClosestTile(): void {
            let _minDist: number = CommonUtils.distanceBetweenPoints(this.position, this._tile.position);
            let _closestTile: TileModel = this._tile;
            for (let tileNeighbor of this._tile.neighborsAsVector) {
                if (tileNeighbor.gameObject) continue;

                let _dist: number = CommonUtils.distanceBetweenPoints(this.position, tileNeighbor.position);
                if (_dist < _minDist) {
                    _minDist = _dist;
                    _closestTile = tileNeighbor;
                }
            }
            if (_closestTile != this._tile) {
                this.tile = _closestTile;
            }
        }

        protected excludeGameObjectsFromHitTestingInUpdateMoving: Array<AbstractGameObjectModel> = null;

        /**
         * @returns {src.model.object.AbstractGameObjectModel} the object intersected with, otherwise null
         */
        public updateMoving(): AbstractGameObjectModel {
            if (this.isMoving) {
                if (!this._tile.neighborsAsDirectionMap[this.direction]) {
                    //simple preventing getting out the battle area
                    return this;
                }

                let prevPosX: number = this.position.x;
                let prevPosY: number = this.position.y;

                this.position.x += this.directionSpeedsValueMap[this.direction].x;
                this.position.y += this.directionSpeedsValueMap[this.direction].y;

                for (let tileNeighbor of this._tile.neighborsAsVector) {
                    if (tileNeighbor.gameObject) {
                        if (this.excludeGameObjectsFromHitTestingInUpdateMoving
                            && this.excludeGameObjectsFromHitTestingInUpdateMoving.indexOf(tileNeighbor.gameObject) >= 0) {

                            continue;
                        }

                        if (this.hitTestWith(tileNeighbor.gameObject)) {
                            this.position.set(prevPosX, prevPosY);

                            return tileNeighbor.gameObject;
                        }
                    }

                }

                this.findClosestTile();
            }

            return null;
        }

    }

    export enum Direction {
        RIGHT = 0,
        LEFT = Math.PI,
        UP = -Math.PI / 2,
        DOWN = Math.PI / 2
    }

    const allDirections:Array<Direction> = [Direction.RIGHT, Direction.LEFT, Direction.UP, Direction.DOWN];

    export function getRandomDirection():Direction {
        return CommonUtils.getRandomItem(allDirections);
    }
}