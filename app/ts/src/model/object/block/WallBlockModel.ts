///<reference path="AbstractBlockModel.ts"/>
module src.model.object.block {
    import TankBulletModel = src.model.object.bullet.TankBulletModel;

    export class WallBlockModel extends AbstractBlockModel {

        constructor() {
            super();
        }

        public hitByBullet(bullet:TankBulletModel):boolean {
            return false;
        }

    }
}