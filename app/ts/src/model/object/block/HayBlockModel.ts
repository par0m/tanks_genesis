///<reference path="AbstractBlockModel.ts"/>
module src.model.object.block {
    import TankBulletModel = src.model.object.bullet.TankBulletModel;

    export class HayBlockModel extends AbstractBlockModel {

        protected fullHealth:number;
        protected health:number;

        constructor(fullHealth:number = 100.0) {
            super();
            this.health = this.fullHealth = fullHealth;
        }

        /**
         * @returns {number} health state as a value [0..1]
         */
        public getHealthState():number {
            return this.health / this.fullHealth;
        }

        public hitByBullet(bullet:TankBulletModel):boolean {
            this.health -= bullet.damage;
            if (this.health < 0) this.health = 0;

            return this.health == 0;
        }

    }
}