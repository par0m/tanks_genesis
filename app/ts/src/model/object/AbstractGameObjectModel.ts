module src.model.object {
    import TankBulletModel = src.model.object.bullet.TankBulletModel;

    export abstract class AbstractGameObjectModel {

        public get viewBounds(): PIXI.Rectangle {
            return this._viewBounds;
        }
        public set viewBounds(_viewBounds: PIXI.Rectangle) {
            this._viewBounds = _viewBounds;
        }

        public set tile(_tile: TileModel) {
            if (this._tile != null) {
                this._tile.gameObject = null;
            }
            this._tile = _tile;
            if (this._tile) {
                this._tile.gameObject = this;
            }
        }

        public get position(): PIXI.Point {
            return this._position;
        }

        protected _tile:TileModel = null;
        private _position:PIXI.Point;
        protected _viewBounds:PIXI.Rectangle;


        constructor() {
            this._position = new PIXI.Point();
        }

        /**
         * @param {src.model.object.bullet.TankBulletModel} bullet
         * @returns {boolean} returns true if this is destroyed by bullet, otherwise false
         */
        public abstract hitByBullet(bullet:TankBulletModel):boolean;


    }
}