module src.model.object.tank {
    export class BlueTankModel extends TankModel {


        constructor() {
            super([
                new PIXI.Point(0.5,-0.25),
                new PIXI.Point(0.5,0),
                new PIXI.Point(0.5,0.25),
            ], Config.BLUE_TANK_BULLET_DAMAGE, 1, 3.5, 60);

        }
    }
}