///<reference path="TankModel.ts"/>
module src.model.object.tank {
    export class RedTankModel extends TankModel {


        constructor() {
            super([
                new PIXI.Point(0.5,-0.15),
                new PIXI.Point(0.5,0.15)
            ], Config.RED_TANK_BULLET_DAMAGE, 1.5, 5, 50);
        }
    }
}