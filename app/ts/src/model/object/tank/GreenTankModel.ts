///<reference path="TankModel.ts"/>
module src.model.object.tank {
    export class GreenTankModel extends TankModel {


        constructor() {
            super([
                new PIXI.Point(0.5, 0)
            ], Config.GREEN_TANK_BULLET_DAMAGE, 2, 4.5, 40);

        }
    }
}