///<reference path="../AbstractGameObjectModel.ts"/>
///<reference path="../../../util/CommonUtils.ts"/>
///<reference path="../MovableGameObject.ts"/>
///<reference path="../bullet/TankBulletModel.ts"/>
module src.model.object.tank {
    import TankBulletModel = src.model.object.bullet.TankBulletModel;

    export class TankModel extends MovableGameObject {
        protected _shootingStartBulletLocalRelativePositions: Array<PIXI.Point>;
        private directionToShootingStartBulletLocalRelativePositionsMapArray:Array<Object>;

        private bulletAxleSpeedModule:number;
        private bulletDamage:number;
        private _isShooting:boolean = false;
        protected _reloadingDurationInUpdates: number;
        private _currentReloadingUpdate:number = 0;

        public get isShooting(): boolean {
            return this._isShooting;
        }

        public set isShooting(_isShooting: boolean) {
            this._isShooting = _isShooting;
        }

        public get viewBounds(): PIXI.Rectangle {
            return this._viewBounds;
        }

        public set viewBounds(_viewBounds: PIXI.Rectangle) {
            this._viewBounds = _viewBounds;

            this.directionToShootingStartBulletLocalRelativePositionsMapArray = [];
            for (let _p of this._shootingStartBulletLocalRelativePositions) {
                let _absoluteP:PIXI.Point = new PIXI.Point(
                    _p.x * this._viewBounds.width,
                    _p.y * this._viewBounds.height
                );

                let _map:Object = new Object();
                _map[Direction.RIGHT] = new PIXI.Point(_absoluteP.x, _absoluteP.y);
                _map[Direction.LEFT] = new PIXI.Point(-_absoluteP.x, -_absoluteP.y);
                _map[Direction.DOWN] = new PIXI.Point(-_absoluteP.y, _absoluteP.x);
                _map[Direction.UP] = new PIXI.Point(_absoluteP.y, -_absoluteP.x);
                this.directionToShootingStartBulletLocalRelativePositionsMapArray.push(_map);
            }
        }



        constructor(_shootingStartBulletLocalRelativePositions:Array<PIXI.Point>, bulletDamage:number,
                    axleSpeedModule:number = 1, bulletAxleSpeedModule: number = 3, _reloadingDurationInUpdates:number = 60) {
            super(axleSpeedModule, Direction.RIGHT);
            this.bulletAxleSpeedModule = bulletAxleSpeedModule;
            this._shootingStartBulletLocalRelativePositions = _shootingStartBulletLocalRelativePositions;
            this.bulletDamage = bulletDamage;
            this._reloadingDurationInUpdates = _reloadingDurationInUpdates;
        }

        public updateShooting(): Array<TankBulletModel> {
            let tankBulletModels:Array<TankBulletModel> = null;
            if (this.isShooting) {
                if (this._currentReloadingUpdate == 0) {
                    tankBulletModels = [];
                    for (let _map of this.directionToShootingStartBulletLocalRelativePositionsMapArray) {
                        let bulletPostion:PIXI.Point = this.position.clone();
                        bulletPostion.x += _map[this.direction].x;
                        bulletPostion.y += _map[this.direction].y;

                        tankBulletModels.push(new TankBulletModel(this, this._tile,
                            bulletPostion, this.bulletAxleSpeedModule, this.direction, this.bulletDamage));
                    }
                }

                this._currentReloadingUpdate = (this._currentReloadingUpdate + 1) % this._reloadingDurationInUpdates;
            } else {
                if (this._currentReloadingUpdate != 0) {
                    this._currentReloadingUpdate = (this._currentReloadingUpdate + 1) % this._reloadingDurationInUpdates;
                }
            }

            return tankBulletModels;
        }

        public hitByBullet(bullet:TankBulletModel):boolean {
            return true;
        }

    }


}