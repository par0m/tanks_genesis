///<reference path="../AbstractGameObjectModel.ts"/>
module src.model.object.bullet {
    import TankModel = src.model.object.tank.TankModel;

    export class TankBulletModel extends MovableGameObject {

        private _damage:number;

        public set tile(_tile: TileModel) {
            this._tile = _tile;
        }

        constructor(tankModel:TankModel, tile:TileModel, position:PIXI.Point, axleSpeedModule: number, _direction:Direction, _damage:number) {
            super(axleSpeedModule, _direction);
            this.excludeGameObjectsFromHitTestingInUpdateMoving = [tankModel];
            this.tile = tile;
            this.position.set(position.x, position.y);
            this._damage = _damage;

            this.isMoving = true;

        }

        public get damage(): number {
            return this._damage;
        }

        public hitByBullet(bullet:TankBulletModel):boolean {
            return false;
        }
    }
}