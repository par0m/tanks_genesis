///<reference path="../../util/CommonUtils.ts"/>
module src.model.object {

    export class TileModel {

        private _position:PIXI.Point;
        private _gameObject:AbstractGameObjectModel;
        private _neighborsAsVector:Array<TileModel>;
        /**
         * _neighborsAsDirectionMap[Direction] == someNeighborTileModel
         */
        private _neighborsAsDirectionMap:Object;

        constructor(iMatrixPosition:number, jMatrixPosition:number, size:number) {
            this._position = new PIXI.Point(iMatrixPosition * size, jMatrixPosition * size);
        }

        public get neighborsAsDirectionMap(): Object {
            return this._neighborsAsDirectionMap;
        }

        public set neighborsAsDirectionMap(_neighborsAsDirectionMap: Object) {
            this._neighborsAsDirectionMap = _neighborsAsDirectionMap;
        }

        public get neighborsAsVector(): Array<src.model.object.TileModel> {
            return this._neighborsAsVector;
        }

        public set neighborsAsVector(_neighbors: Array<src.model.object.TileModel>) {
            this._neighborsAsVector = _neighbors;
        }

        public set gameObject(_gameObject: AbstractGameObjectModel) {
            this._gameObject = _gameObject;
        }
        public get gameObject(): AbstractGameObjectModel {
            return this._gameObject;
        }

        public get position(): PIXI.Point {
            return this._position;
        }

    }
}