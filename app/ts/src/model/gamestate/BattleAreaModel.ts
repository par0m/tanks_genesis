module src.model.gamestate {
    import TankModel = src.model.object.tank.TankModel;
    import Direction = src.model.object.Direction;
    import TankBulletModel = src.model.object.bullet.TankBulletModel;
    import AbstractGameObjectModel = src.model.object.AbstractGameObjectModel;
    import TileModel = src.model.object.TileModel;
    import CommonUtils = src.util.CommonUtils;
    import getRandomDirection = src.model.object.getRandomDirection;

    export class BattleAreaModel {

        private _tilesMatrix:Array<Array<model.object.TileModel>>;
        private _tanks:Array<TankModel>;

        private _playerTank:TankModel = null;
        private _tankBullets:Array<TankBulletModel> = [];

        private _onTankBulletShotCallback:(tbm:TankBulletModel) => void = null;
        private _onGameObjectDestroyedCallback:(agom:AbstractGameObjectModel) => void = null;

        constructor(tilesMatrix:Array<Array<model.object.TileModel>>, tanks:Array<TankModel>) {
            this._tilesMatrix = tilesMatrix;
            this._tanks = tanks;
            this.nextPlayerTank();

            this._tanks[0].isShooting=true;
            this._tanks[1].isShooting=true;
        }

        public get tilesMatrix(): Array<Array<src.model.object.TileModel>> {
            return this._tilesMatrix;
        }

        public set onTankBulletShotCallback(_onTankBulletShotCallback: (tbm: TankBulletModel) => void) {
            this._onTankBulletShotCallback = _onTankBulletShotCallback;
        }

        public set onGameObjectDestroyedCallback(_onGameObjectDestroyedCallback
                                                     : (agom: src.model.object.AbstractGameObjectModel) => void) {
            this._onGameObjectDestroyedCallback = _onGameObjectDestroyedCallback;
        }

        public nextPlayerTank():void {
            if (this._playerTank) {
                if (this._tanks.length < 2) {
                    //It doesn't make any sense to change the same tank
                    return;
                }

                this.setPlayerTankMoving(false);
                this.setPlayerTankShooting(false);
            }


            this._playerTank = this._tanks.shift();
            this._tanks.push(this._playerTank);
        }

        public setPlayerTankDirection(direction:Direction):void {
            this._playerTank.direction = direction;
        }
        public setPlayerTankMoving(isMoving:boolean):void {
            this._playerTank.isMoving = isMoving;
        }
        public setPlayerTankShooting(isShooting:boolean):void {
            this._playerTank.isShooting = isShooting;
        }

        public updateModel():void {
            this.updateSimpleRandomBasedAI();

            for (let _tank of this._tanks) {
                _tank.updateMoving();

                let tankBulletModels:Array<TankBulletModel> = _tank.updateShooting();
                if (tankBulletModels) {
                    this._tankBullets = this._tankBullets.concat(tankBulletModels);
                    for (let tbm of tankBulletModels) {
                        if (this._onTankBulletShotCallback) this._onTankBulletShotCallback(tbm);
                    }
                }
            }

            for (let i:number = this._tankBullets.length - 1; i >= 0; i--) {
                let intersectedWith:AbstractGameObjectModel = this._tankBullets[i].updateMoving();
                if (intersectedWith) {
                    let destroyed:boolean = intersectedWith.hitByBullet(this._tankBullets[i]);
                    if (destroyed) {
                        if (intersectedWith instanceof TankModel) {
                            let rndFreeTile:TileModel = this.getRandomFreeTile();
                            if (rndFreeTile) {
                                intersectedWith.tile = rndFreeTile;
                                intersectedWith.position.set(rndFreeTile.position.x, rndFreeTile.position.y);
                            } else {
                                alert('NO FREE TILES! RELOAD PAGE!');
                            }
                        } else {
                            intersectedWith.tile = null;
                            this._onGameObjectDestroyedCallback(intersectedWith);
                        }
                    }
                    this._onGameObjectDestroyedCallback(this._tankBullets[i]);
                    this._tankBullets[i].tile = null;
                    this._tankBullets.splice(i, 1);
                }
            }
        }

        private getRandomFreeTile():TileModel {
            let freeTiles:Array<TileModel> = [];
            for (let _row of this.tilesMatrix) {
                for (let _tile of _row) {
                   if (_tile.gameObject == null) {
                       freeTiles.push(_tile);
                   }
                }
            }
            return CommonUtils.getRandomItem(freeTiles);
        }
        //------------------SIMPLE AI IMITATION-----------------------//
        protected updateSimpleRandomBasedAI():void {
            for (let _tank of this._tanks) {
                if (_tank != this._playerTank) {
                    if (_tank.isMoving) {
                        if (Math.random() > 0.99) {
                            _tank.isMoving = false;
                        }
                    } else {
                        if (Math.random() > 0.1) {
                            _tank.isMoving = true;
                        }
                    }


                    if (Math.random() > 0.99) {
                        _tank.direction = getRandomDirection();
                    }

                    if (_tank.isShooting) {
                        if (Math.random() > 0.98) {
                            _tank.isShooting = false;
                        }
                    } else {
                        if (Math.random() > 0.85) {
                            _tank.isShooting = true;
                        }
                    }
                }
            }

        }
    }
}