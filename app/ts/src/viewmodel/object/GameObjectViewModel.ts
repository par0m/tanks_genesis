module src.viewmodel.object {
    import AbstractGameObjectModel = src.model.object.AbstractGameObjectModel;
    import GameObjectView = src.view.object.GameObjectView;
    import MovableGameObject = src.model.object.MovableGameObject;
    import HayBlockModel = src.model.object.block.HayBlockModel;
    import GameObjectViewPool = src.factory.pool.GameObjectViewPool;
    import TankModel = src.model.object.tank.TankModel;

    export class GameObjectViewModel {

        protected static readonly GAME_OBJECTS_MODEL_PREFIX_VIEW_NAME_MAP:Object = {
            'RedTankModel' : '_REDTANK_',
            'BlueTankModel' : '_BLUETANK_',
            'GreenTankModel' : '_GREENTANK_',
            'TankBulletModel' : '_BULLET_',
            'HayBlockModel' : '_HAYBLOCK_',
            'WallBlockModel' : '_WALLBLOCK_'
        };
        /**
         * Retrieves a GameObjcetView for its model from the singleton pool GameObjectViewPool
         * and binds with he model
         * @param gmeObjectModel
         */
        public static retrieveGameObjectView(gameObjectModel:AbstractGameObjectModel):GameObjectView {
            let gameObjectView:GameObjectView = GameObjectViewPool.instance.getElement(
                GameObjectViewModel.GAME_OBJECTS_MODEL_PREFIX_VIEW_NAME_MAP[gameObjectModel.constructor['name']],
                true
            );
            return gameObjectView;
        }

        public static bind(gameObjectView:GameObjectView, gameObjectModel:AbstractGameObjectModel):GameObjectViewModel {
            gameObjectView.position.set(gameObjectModel.position.x, gameObjectModel.position.y);
            gameObjectModel.viewBounds = gameObjectView.getBounds();
            let gameObjectViewModel:GameObjectViewModel = new GameObjectViewModel(gameObjectView, gameObjectModel);
            return gameObjectViewModel;
        }


        view:GameObjectView;
        model:AbstractGameObjectModel;

        constructor(view: src.view.object.GameObjectView, model: src.model.object.AbstractGameObjectModel) {
            this.view = view;
            this.model = model;

            if (this.model instanceof TankModel) {
                this.synchronize = () => {
                    this.view.position.set(this.model.position.x, this.model.position.y);
                    this.view.rotation = (<MovableGameObject>this.model).direction;
                };
            } else if (this.model instanceof MovableGameObject) {
                this.synchronize = () => {
                    this.view.position.set(this.model.position.x, this.model.position.y);
                };
            } else if (this.model instanceof HayBlockModel) {
                this.synchronize = () => {
                    this.view.alpha = (<HayBlockModel>this.model).getHealthState();
                };
            }

        }

        public synchronize: () => void = null;
    }
}