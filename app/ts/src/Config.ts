module src {
    export class Config {
        static readonly FONT:string = "Geo";

        static readonly TILE_SIZE_PX:number = 35;
        static readonly MAP_TILES_HORIZONTAL_NUM:number = 50;
        static readonly MAP_TILES_VERTICAL_NUM:number = 50;

        static readonly RED_TANK_BULLET_DAMAGE:number = 10;
        static readonly GREEN_TANK_BULLET_DAMAGE:number = 25;
        static readonly BLUE_TANK_BULLET_DAMAGE:number = 20;

        /**
         * created to provide control's changing on runtime
         * @type {{TANK_DIRECTION_LEFT: number; TANK_DIRECTION_RIGHT: number; TANK_DIRECTION_UP: number; TANK_DIRECTION_DOWN: number; NEXT_PLAYER_TANK: number; TANK_SHOOTING: number}}
         */
        static readonly KEYCODES:Object = {
            'TANK_DIRECTION_LEFT': 37,
            'TANK_DIRECTION_RIGHT': 39,
            'TANK_DIRECTION_UP': 38,
            'TANK_DIRECTION_DOWN': 40,

            'NEXT_PLAYER_TANK': 67, //key 'c' by default
            'TANK_SHOOTING': 32, //key 'space' by default
        };
    }

    export enum TEXTURE_NAME_CONFIG {
        GAME_TEXTURE = 'assets/img/gameTexture.json'
    }
}