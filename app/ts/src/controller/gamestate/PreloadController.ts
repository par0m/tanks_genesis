module src.controler.gamestate {

    export class PreloadController {

        private preloadView:view.gamestate.PreloadView;

        constructor(preloadView:view.gamestate.PreloadView) {
            this.preloadView = preloadView;
        }

        public startPreload():void {
            this.preloadView.show();

            PIXI.loader
                .add(TEXTURE_NAME_CONFIG.GAME_TEXTURE)
                .on('progress', (loader:PIXI.loaders.Loader, resources:PIXI.loaders.Resource) => {
                    this.preloadView.updateLoadingProgress(loader.progress);
                }, this)
                .load((loader:PIXI.loaders.Loader, resources:PIXI.loaders.Resource) => {
                    this.preloadView.hide();

                    src.factory.gamestate.GameStatesFactory.instance.createBattleAreaController().startBattle();
                });

        }




    }
}