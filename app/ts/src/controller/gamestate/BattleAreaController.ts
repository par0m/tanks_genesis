///<reference path="../../factory/pool/GameObjectViewPool.ts"/>
///<reference path="../../model/object/tank/TankModel.ts"/>
///<reference path="../../model/object/block/HayBlockModel.ts"/>
///<reference path="../../viewmodel/object/GameObjectViewModel.ts"/>

module src.controler.gamestate {
    import BattleAreaModel = src.model.gamestate.BattleAreaModel;
    import BattleAreaView = src.view.gamestate.BattleAreaView;
    import AbstractGameObjectModel = src.model.object.AbstractGameObjectModel;
    import Direction = src.model.object.Direction;
    import TankBulletModel = src.model.object.bullet.TankBulletModel;
    import GameObjectView = src.view.object.GameObjectView;
    import GameObjectViewModel = src.viewmodel.object.GameObjectViewModel;

    export class BattleAreaController {


        private battleAreModel: BattleAreaModel;
        private battleAreaView: BattleAreaView;

        private gameObjectsViewModel: Array<GameObjectViewModel>;

        private app: PIXI.Application;


        constructor(battleAreModel: BattleAreaModel, battleAreaView: BattleAreaView,
                    gameObjectViewModelsArray: Array<GameObjectViewModel>, app: PIXI.Application) {
            this.battleAreModel = battleAreModel;
            this.battleAreaView = battleAreaView;
            this.gameObjectsViewModel = gameObjectViewModelsArray;
            this.app = app;
        }

        public startBattle(): void {
            this.battleAreaView.show();

            for (let viewModel of this.gameObjectsViewModel) {
                this.battleAreaView.addGameObjectView(viewModel.view);
            }

            this.battleAreModel.onTankBulletShotCallback = this.tankBulletShootHandler;
            this.battleAreModel.onGameObjectDestroyedCallback = this.gameObjectDestructionHandler;

            window.addEventListener('keydown', this.keyDownHandler);
            window.addEventListener('keyup', this.keyUpHandler);

            this.app.ticker.add(this.updateController, this);
        }

        public tankBulletShootHandler: (tbm: TankBulletModel) => void = (tbm: TankBulletModel) => {
            let gameObjectView:GameObjectView = GameObjectViewModel.retrieveGameObjectView(tbm);
            let gameObjectViewModel:GameObjectViewModel = GameObjectViewModel.bind(gameObjectView, tbm);
            this.gameObjectsViewModel.push(gameObjectViewModel);
            this.battleAreaView.addGameObjectView(gameObjectViewModel.view);
        };

        public gameObjectDestructionHandler: (agom: src.model.object.AbstractGameObjectModel) => void
                                            = (agom: src.model.object.AbstractGameObjectModel) => {
            this.battleAreaView.removeGameObjectView(this.removeViewModelBy(agom).view);
        };

        private getViewModelBy(model:AbstractGameObjectModel):GameObjectViewModel {
            for (let viewModel of this.gameObjectsViewModel) {
                if (viewModel.model == model) {
                    return viewModel;
                }
            }
            return null;
        }
        private removeViewModelBy(model:AbstractGameObjectModel): GameObjectViewModel {
            let vm:GameObjectViewModel = this.getViewModelBy(model);
            if (vm) {
                this.gameObjectsViewModel.splice(this.gameObjectsViewModel.indexOf(vm), 1);
            }
            return vm;
        }


        private isPressed: Object = {
            'DIRECTION': {
                'LEFT': false,
                'RIGHT': false,
                'UP': false,
                'DOWN': false
            },
            'SHOOTING': false

        };
        protected keyDownHandler: (e: KeyboardEvent) => void = (e: KeyboardEvent) => {
            switch (e.keyCode) {
                case Config.KEYCODES['TANK_DIRECTION_LEFT']:
                    this.isPressed['DIRECTION']['LEFT'] = true;
                    break;
                case Config.KEYCODES['TANK_DIRECTION_RIGHT']:
                    this.isPressed['DIRECTION']['RIGHT'] = true;
                    break;
                case Config.KEYCODES['TANK_DIRECTION_UP']:
                    this.isPressed['DIRECTION']['UP'] = true;
                    break;
                case Config.KEYCODES['TANK_DIRECTION_DOWN']:
                    this.isPressed['DIRECTION']['DOWN'] = true;
                    break;
                case Config.KEYCODES['TANK_SHOOTING']:
                    this.isPressed['SHOOTING'] = true;
                    break;
            }
        };
        protected keyUpHandler: (e: KeyboardEvent) => void = (e: KeyboardEvent) => {
            switch (e.keyCode) {
                case Config.KEYCODES['TANK_DIRECTION_LEFT']:
                    this.isPressed['DIRECTION']['LEFT'] = false;
                    break;
                case Config.KEYCODES['TANK_DIRECTION_RIGHT']:
                    this.isPressed['DIRECTION']['RIGHT'] = false;
                    break;
                case Config.KEYCODES['TANK_DIRECTION_UP']:
                    this.isPressed['DIRECTION']['UP'] = false;
                    break;
                case Config.KEYCODES['TANK_DIRECTION_DOWN']:
                    this.isPressed['DIRECTION']['DOWN'] = false;
                    break;
                case Config.KEYCODES['TANK_SHOOTING']:
                    this.isPressed['SHOOTING'] = false;
                    break;
                case Config.KEYCODES['NEXT_PLAYER_TANK']:
                    this.battleAreModel.nextPlayerTank();
                    break;
            }
        };


        public stopBattle(): void {
            this.battleAreaView.hide();

            window.removeEventListener('keydown', this.keyDownHandler);
            window.removeEventListener('keyup', this.keyUpHandler);

            this.battleAreModel.onTankBulletShotCallback = null;

            this.app.ticker.remove(this.updateController, this);
        }

        private updateKeyboardInteractivityActions(): void {
            let pressedDirectionKeysNum: number = 0;
            let pressedKeyDirection: string = null;
            for (let keyDirection in this.isPressed['DIRECTION']) {
                if (this.isPressed['DIRECTION'][keyDirection]) {
                    pressedDirectionKeysNum++;
                    pressedKeyDirection = keyDirection;
                }
            }
            if (pressedDirectionKeysNum == 1) {
                this.battleAreModel.setPlayerTankDirection(Direction[pressedKeyDirection]);
                this.battleAreModel.setPlayerTankMoving(true);
            } else {
                this.battleAreModel.setPlayerTankMoving(false);
            }

            this.battleAreModel.setPlayerTankShooting(this.isPressed['SHOOTING']);

        }

        protected updateController(): void {
            this.updateKeyboardInteractivityActions();

            this.battleAreModel.updateModel();

            for (var i: number = 0; i < this.gameObjectsViewModel.length; i++) {
                if (this.gameObjectsViewModel[i].synchronize) {
                    this.gameObjectsViewModel[i].synchronize();
                }
            }

            this.battleAreaView.updateView();

        }
    }

}