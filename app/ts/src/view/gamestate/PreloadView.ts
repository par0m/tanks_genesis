module  src.view.gamestate {
    export class PreloadView extends AbstractGameStateView {

        private indicator:PIXI.Text;

        constructor(app:PIXI.Application) {
            super(app);
        }

        public show():void {
            this.indicator = new PIXI.Text('0%', {
                    fontFamily : Config.FONT,
                    fontSize: 30,
                    fill : 0x000000,
                    align : 'center'
                });
            this.indicator.position.set(this.app.renderer.width / 2, this.app.renderer.height / 2);
            this.indicator.anchor.set(0.5, 0.5);
            this.addChild(this.indicator);
            this.app.stage.addChild(this);
        }

        public hide():void {
            this.parent.removeChild(this);
        }

        /**
         *
         * @param {number} progress, values 0..100
         */
        public updateLoadingProgress(progress:number):void {
            this.indicator.text = progress + '%';
        }
    }
}