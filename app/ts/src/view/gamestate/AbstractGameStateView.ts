module  src.view.gamestate {
    export abstract class AbstractGameStateView extends PIXI.Container {

        protected app:PIXI.Application;

        constructor(app:PIXI.Application) {
            super();
            this.app = app;
        }

        public abstract show():void;
        public abstract hide():void;

    }
}