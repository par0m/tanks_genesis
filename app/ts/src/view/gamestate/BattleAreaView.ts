module src.view.gamestate {

    export class BattleAreaView extends AbstractGameStateView {
        constructor(app:PIXI.Application) {
            super(app);
        }

        public show():void {
            this.app.stage.addChild(this);

            //UI control hint
            let controlHint:PIXI.Text = new PIXI.Text(
                '[ARROW KEYS] - MOVE\n'+
                '[SPACE] - SHOOTING\n'+
                '[C] - CHANGE TANK\n'
                , {
                fontFamily : Config.FONT,
                fontSize: 25,
                fill : 0x000000,
                align : 'center'
            });
            controlHint.position.set(100, 50);
            controlHint.anchor.set(0.5, 0.5);
            this.addChild(controlHint);
        }

        public hide():void {
            this.parent.removeChild(this);
        }

        public addGameObjectView(gameObjcetView:view.object.GameObjectView):void {
            this.addChildAt(gameObjcetView, this.children.length - 1/*because of UI control hint*/);
        }

        public removeGameObjectView(gameObjcetView:view.object.GameObjectView):void {
            this.removeChild(gameObjcetView);
        }

        public updateView():void {

        }
    }
}