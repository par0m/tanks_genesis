module src.view.object {
    import Sprite = PIXI.Sprite;
    import CommonUtils = src.util.CommonUtils;

    export class GameObjectView extends Sprite {
        private _prefix:string;

        public get prefix(): string {
            return this._prefix;
        }

        constructor(prefix:string) {
            super(CommonUtils.getSingleFrameTextrure(TEXTURE_NAME_CONFIG.GAME_TEXTURE, prefix));
            this._prefix = prefix;
            this.anchor.set(0.5, 0.5);
        }

    }
}