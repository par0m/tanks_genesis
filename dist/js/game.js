var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var src;
(function (src) {
    var Config = (function () {
        function Config() {
        }
        Config.FONT = "Geo";
        Config.TILE_SIZE_PX = 35;
        Config.MAP_TILES_HORIZONTAL_NUM = 50;
        Config.MAP_TILES_VERTICAL_NUM = 50;
        Config.RED_TANK_BULLET_DAMAGE = 10;
        Config.GREEN_TANK_BULLET_DAMAGE = 25;
        Config.BLUE_TANK_BULLET_DAMAGE = 20;
        /**
         * created to provide control's changing on runtime
         * @type {{TANK_DIRECTION_LEFT: number; TANK_DIRECTION_RIGHT: number; TANK_DIRECTION_UP: number; TANK_DIRECTION_DOWN: number; NEXT_PLAYER_TANK: number; TANK_SHOOTING: number}}
         */
        Config.KEYCODES = {
            'TANK_DIRECTION_LEFT': 37,
            'TANK_DIRECTION_RIGHT': 39,
            'TANK_DIRECTION_UP': 38,
            'TANK_DIRECTION_DOWN': 40,
            'NEXT_PLAYER_TANK': 67,
            'TANK_SHOOTING': 32,
        };
        return Config;
    }());
    src.Config = Config;
    var TEXTURE_NAME_CONFIG;
    (function (TEXTURE_NAME_CONFIG) {
        TEXTURE_NAME_CONFIG["GAME_TEXTURE"] = "assets/img/gameTexture.json";
    })(TEXTURE_NAME_CONFIG = src.TEXTURE_NAME_CONFIG || (src.TEXTURE_NAME_CONFIG = {}));
})(src || (src = {}));
var src;
(function (src) {
    var Main = (function () {
        function Main() {
            //creation and configuration the application
            this.app = new PIXI.Application(window.innerWidth, window.innerHeight);
            this.app.renderer.backgroundColor = 0xAAFFF6;
            var stageScale = Math.min(window.innerWidth / (src.Config.MAP_TILES_HORIZONTAL_NUM * src.Config.TILE_SIZE_PX), window.innerHeight / (src.Config.MAP_TILES_VERTICAL_NUM * src.Config.TILE_SIZE_PX));
            this.app.stage.scale.set(stageScale, stageScale);
            document.body.appendChild(this.app.view);
            this.app.view.style.marginTop = (window.innerHeight -
                (src.Config.MAP_TILES_HORIZONTAL_NUM * src.Config.TILE_SIZE_PX) * stageScale) / 2 + 'px';
            this.app.view.style.marginLeft = (window.innerWidth -
                (src.Config.MAP_TILES_VERTICAL_NUM * src.Config.TILE_SIZE_PX) * stageScale) / 2 + 'px';
            //start game logic
            new src.factory.gamestate.GameStatesFactory(this.app)
                .createPreloadController()
                .startPreload();
        }
        return Main;
    }());
    src.Main = Main;
})(src || (src = {}));
var game;
window.onload = function () {
    game = new src.Main();
};
var src;
(function (src) {
    var factory;
    (function (factory) {
        var pool;
        (function (pool) {
            /**
             * Pulling based on existing in PIXI display list
             * @author Roman
             */
            var AbstractDisplayObjectPool = (function () {
                function AbstractDisplayObjectPool() {
                    this.elements = [];
                    this.freeElements = [];
                }
                AbstractDisplayObjectPool.prototype.moveToElements = function (target) {
                    var _this = this;
                    if (target === void 0) { target = null; }
                    target.off(AbstractDisplayObjectPool.ADDED);
                    this.move(target, this.freeElements, this.elements);
                    target.on(AbstractDisplayObjectPool.REMOVED, function () {
                        _this.moveToFreeElements(target);
                    }, this);
                };
                AbstractDisplayObjectPool.prototype.moveToFreeElements = function (target) {
                    var _this = this;
                    target.off(AbstractDisplayObjectPool.REMOVED);
                    this.move(target, this.elements, this.freeElements);
                    target.on(AbstractDisplayObjectPool.ADDED, function () {
                        _this.moveToElements(target);
                    }, this);
                };
                AbstractDisplayObjectPool.prototype.move = function (element, fromArray, toArray) {
                    var index;
                    if ((index = fromArray.indexOf(element)) > -1) {
                        fromArray.splice(index, 1);
                    }
                    if ((index = toArray.indexOf(element)) == -1) {
                        toArray.push(element);
                    }
                };
                AbstractDisplayObjectPool.prototype.destroy = function (everyElementCallback) {
                    if (everyElementCallback === void 0) { everyElementCallback = null; }
                    for (var i = this.elements.length - 1; i > -1; i--) {
                        this.elements[i].parent.removeChild(this.elements[i]);
                    }
                    if (everyElementCallback) {
                        for (var j = 0; j < this.freeElements.length; j++) {
                            everyElementCallback(this.freeElements[j]);
                        }
                    }
                    this.freeElements.splice(0, this.freeElements.length);
                };
                AbstractDisplayObjectPool.ADDED = 'added';
                AbstractDisplayObjectPool.REMOVED = 'removed';
                return AbstractDisplayObjectPool;
            }());
            pool.AbstractDisplayObjectPool = AbstractDisplayObjectPool;
        })(pool = factory.pool || (factory.pool = {}));
    })(factory = src.factory || (src.factory = {}));
})(src || (src = {}));
///<reference path="AbstractDisplayObjectPool.ts"/>
var src;
(function (src) {
    var factory;
    (function (factory) {
        var pool;
        (function (pool) {
            var GameObjectViewPool = (function (_super) {
                __extends(GameObjectViewPool, _super);
                function GameObjectViewPool() {
                    return _super.call(this) || this;
                }
                Object.defineProperty(GameObjectViewPool, "instance", {
                    get: function () {
                        return GameObjectViewPool._instance ? GameObjectViewPool._instance
                            : GameObjectViewPool._instance = new GameObjectViewPool();
                    },
                    enumerable: true,
                    configurable: true
                });
                /**
                 *
                 * @param {string} prefix will be used to create Texture and Sprite. See new view.object.GameObjectView(prefix)
                 * @param {boolean} setAsUsing if true retrieved object will be used even if it is not added in PIXI display list
                 * @returns {src.view.object.GameObjectView} retrieved object
                 */
                GameObjectViewPool.prototype.getElement = function (prefix, setAsUsed) {
                    var _this = this;
                    if (setAsUsed === void 0) { setAsUsed = false; }
                    var object = null;
                    for (var i = 0; i < this.freeElements.length; i++) {
                        if (this.freeElements[i].prefix == prefix) {
                            object = this.freeElements[i];
                            break;
                        }
                    }
                    if (!object) {
                        object = new src.view.object.GameObjectView(prefix);
                        this.freeElements.push(object);
                        object.on(pool.AbstractDisplayObjectPool.ADDED, function () {
                            _this.moveToElements(object);
                        }, this);
                    }
                    if (setAsUsed) {
                        this.moveToElements(object);
                        object.off(pool.AbstractDisplayObjectPool.ADDED);
                    }
                    return object;
                };
                GameObjectViewPool._instance = null;
                return GameObjectViewPool;
            }(pool.AbstractDisplayObjectPool));
            pool.GameObjectViewPool = GameObjectViewPool;
        })(pool = factory.pool || (factory.pool = {}));
    })(factory = src.factory || (src.factory = {}));
})(src || (src = {}));
var src;
(function (src) {
    var model;
    (function (model) {
        var object;
        (function (object) {
            var AbstractGameObjectModel = (function () {
                function AbstractGameObjectModel() {
                    this._tile = null;
                    this._position = new PIXI.Point();
                }
                Object.defineProperty(AbstractGameObjectModel.prototype, "viewBounds", {
                    get: function () {
                        return this._viewBounds;
                    },
                    set: function (_viewBounds) {
                        this._viewBounds = _viewBounds;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(AbstractGameObjectModel.prototype, "tile", {
                    set: function (_tile) {
                        if (this._tile != null) {
                            this._tile.gameObject = null;
                        }
                        this._tile = _tile;
                        if (this._tile) {
                            this._tile.gameObject = this;
                        }
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(AbstractGameObjectModel.prototype, "position", {
                    get: function () {
                        return this._position;
                    },
                    enumerable: true,
                    configurable: true
                });
                return AbstractGameObjectModel;
            }());
            object.AbstractGameObjectModel = AbstractGameObjectModel;
        })(object = model.object || (model.object = {}));
    })(model = src.model || (src.model = {}));
})(src || (src = {}));
var src;
(function (src) {
    var util;
    (function (util) {
        var CommonUtils = (function () {
            function CommonUtils() {
            }
            /**
             * Returns a texture from the PIXI cache
             * for more info see CommonUtils.generateSingleFrameName
             * @param {src.TEXTURE_NAME_CONFIG} textureName
             * @param {string} prefix  The start of the filename. If the filename was 'explosion_0001-large' the prefix would be 'explosion_'.
             * @returns {PIXI.Texture} texture
             */
            CommonUtils.getSingleFrameTextrure = function (textureName, prefix) {
                return PIXI.loader.resources[textureName]['textures'][CommonUtils.generateSingleFrameName(prefix)];
            };
            /**
             * Generates single frame name for generated by Adobe Animate sprite sheet JSON
             * for more info see CommonUtils.generateFrameNames
             * @param {string} prefix  The start of the filename. If the filename was 'explosion_0001-large' the prefix would be 'explosion_'.
             * @returns {string} frame name
             */
            CommonUtils.generateSingleFrameName = function (prefix) {
                return CommonUtils.generateFrameNames(prefix, 0, 0, "", 4)[0];
            };
            /**
             * Generates frame names for generated by Adobe Animate sprite sheet JSON
             *
             * Really handy function for when you are creating arrays of animation data but it's using frame names and not numbers.
             * For example imagine you've got 30 frames named: 'explosion_0001-large' to 'explosion_0030-large'
             * You could use this function to generate those by doing: Phaser.Animation.generateFrameNames('explosion_', 1, 30, '-large', 4);
             *
             * @param prefix The start of the filename. If the filename was 'explosion_0001-large' the prefix would be 'explosion_'.
             * @param start The number to start sequentially counting from. If your frames are named 'explosion_0001' to 'explosion_0034' the start is 1.
             * @param stop The number to count to. If your frames are named 'explosion_0001' to 'explosion_0034' the stop value is 34.
             * @param suffix The end of the filename. If the filename was 'explosion_0001-large' the prefix would be '-large'. - Default: ''
             * @param zeroPad The number of zeros to pad the min and max values with. If your frames are named 'explosion_0001' to 'explosion_0034' then the zeroPad is 4.
             * @return An array of frame names.
             */
            CommonUtils.generateFrameNames = function (prefix, start, stop, suffix, zeroPad) {
                if (suffix === undefined) {
                    suffix = '';
                }
                var output = [];
                var frame = '';
                if (start < stop) {
                    for (var i = start; i <= stop; i++) {
                        if (typeof zeroPad === 'number') {
                            frame = CommonUtils.pad(i.toString(), zeroPad, '0', 1);
                        }
                        else {
                            frame = i.toString();
                        }
                        frame = prefix + frame + suffix;
                        output.push(frame);
                    }
                }
                else {
                    for (var i = start; i >= stop; i--) {
                        if (typeof zeroPad === 'number') {
                            frame = CommonUtils.pad(i.toString(), zeroPad, '0', 1);
                        }
                        else {
                            frame = i.toString();
                        }
                        frame = prefix + frame + suffix;
                        output.push(frame);
                    }
                }
                return output;
            };
            /**
             * @param str - The target string.
             * @param length=0 - The number of characters to be added.
             * @param pad=" " - The string to pad it out with (defaults to a space).
             * @param dir=3 The direction dir = 1 (left), 2 (right), 3 (both).
             * @return The padded string
             */
            CommonUtils.pad = function (str, length, pad, direction) {
                if (length === void 0) { length = 0; }
                if (pad === void 0) { pad = " "; }
                if (direction === void 0) { direction = 3; }
                var result;
                var padLength = 0;
                if (length + 1 >= str.length) {
                    switch (direction) {
                        case 1:
                            result = new Array(length + 1 - str.length).join(pad) + str;
                            break;
                        case 3:
                            var right = Math.ceil((padLength = length - str.length) / 2);
                            var left = padLength - right;
                            result = new Array(left + 1).join(pad) + str + new Array(right + 1).join(pad);
                            break;
                        default:
                            result = str + new Array(length + 1 - str.length).join(pad);
                            break;
                    }
                }
                return result;
            };
            /**
             * Removes a random object from the given array and returns it.
             *
             * Will return null if there are no array items that fall within the specified range
             * or if there is no item for the randomly chosen index.
             *
             * @param objects An array of objects.
             * @param startIndex Optional offset off the front of the array. Default value is 0, or the beginning of the array.
             * @param length Optional restriction on the number of values you want to randomly select from.
             * @return The random object that was removed.
             */
            CommonUtils.removeRandomItem = function (objects, startIndex, length) {
                if (startIndex === void 0) { startIndex = 0; }
                if (objects == null) {
                    return null;
                }
                if (length === undefined) {
                    length = objects.length;
                }
                var randomIndex = startIndex + Math.floor(Math.random() * length);
                if (randomIndex < objects.length) {
                    var removed = objects.splice(randomIndex, 1);
                    return removed[0] === undefined ? null : removed[0];
                }
                return null;
            };
            /**
             * Returns a random object from the given array
             * @param {T[]} objects An array of objects.
             * @returns {T} random object or null if objects is empty
             */
            CommonUtils.getRandomItem = function (objects) {
                if (objects.length == 0)
                    return null;
                return objects[Math.floor(Math.random() * objects.length)];
            };
            CommonUtils.distanceBetweenPoints = function (p1, p2) {
                return Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));
            };
            return CommonUtils;
        }());
        util.CommonUtils = CommonUtils;
    })(util = src.util || (src.util = {}));
})(src || (src = {}));
///<reference path="../../util/CommonUtils.ts"/>
var src;
(function (src) {
    var model;
    (function (model) {
        var object;
        (function (object) {
            var CommonUtils = src.util.CommonUtils;
            var MovableGameObject = (function (_super) {
                __extends(MovableGameObject, _super);
                function MovableGameObject(axleSpeedModule, direction) {
                    var _this = _super.call(this) || this;
                    _this._isMoving = false;
                    _this.directionSpeedsValueMap = new Object();
                    _this.excludeGameObjectsFromHitTestingInUpdateMoving = null;
                    _this.axleSpeedModule = axleSpeedModule;
                    _this.direction = direction;
                    return _this;
                }
                Object.defineProperty(MovableGameObject.prototype, "isMoving", {
                    get: function () {
                        return this._isMoving;
                    },
                    set: function (_isMoving) {
                        this._isMoving = _isMoving;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(MovableGameObject.prototype, "axleSpeedModule", {
                    set: function (_axleSpeedModule) {
                        this._axleSpeedModule = _axleSpeedModule;
                        this.directionSpeedsValueMap[Direction.RIGHT] = { x: this._axleSpeedModule, y: 0 };
                        this.directionSpeedsValueMap[Direction.LEFT] = { x: -this._axleSpeedModule, y: 0 };
                        this.directionSpeedsValueMap[Direction.UP] = { x: 0, y: -this._axleSpeedModule };
                        this.directionSpeedsValueMap[Direction.DOWN] = { x: 0, y: this._axleSpeedModule };
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(MovableGameObject.prototype, "direction", {
                    get: function () {
                        return this._direction;
                    },
                    set: function (_direction) {
                        this._direction = _direction;
                    },
                    enumerable: true,
                    configurable: true
                });
                MovableGameObject.prototype.hitTestWith = function (otherAbstractGameObjectModel, paddingPercentage) {
                    if (paddingPercentage === void 0) { paddingPercentage = 0.04; }
                    return (Math.abs(this.position.x - otherAbstractGameObjectModel.position.x)
                        < (this.viewBounds.width + otherAbstractGameObjectModel.viewBounds.width) * (0.5 - paddingPercentage)) && (Math.abs(this.position.y - otherAbstractGameObjectModel.position.y)
                        < (this.viewBounds.height + otherAbstractGameObjectModel.viewBounds.height) * (0.5 - paddingPercentage));
                };
                /**
                 * Finds the closest free tile to this GameObject
                 */
                MovableGameObject.prototype.findClosestTile = function () {
                    var _minDist = CommonUtils.distanceBetweenPoints(this.position, this._tile.position);
                    var _closestTile = this._tile;
                    for (var _i = 0, _a = this._tile.neighborsAsVector; _i < _a.length; _i++) {
                        var tileNeighbor = _a[_i];
                        if (tileNeighbor.gameObject)
                            continue;
                        var _dist = CommonUtils.distanceBetweenPoints(this.position, tileNeighbor.position);
                        if (_dist < _minDist) {
                            _minDist = _dist;
                            _closestTile = tileNeighbor;
                        }
                    }
                    if (_closestTile != this._tile) {
                        this.tile = _closestTile;
                    }
                };
                /**
                 * @returns {src.model.object.AbstractGameObjectModel} the object intersected with, otherwise null
                 */
                MovableGameObject.prototype.updateMoving = function () {
                    if (this.isMoving) {
                        if (!this._tile.neighborsAsDirectionMap[this.direction]) {
                            //simple preventing getting out the battle area
                            return this;
                        }
                        var prevPosX = this.position.x;
                        var prevPosY = this.position.y;
                        this.position.x += this.directionSpeedsValueMap[this.direction].x;
                        this.position.y += this.directionSpeedsValueMap[this.direction].y;
                        for (var _i = 0, _a = this._tile.neighborsAsVector; _i < _a.length; _i++) {
                            var tileNeighbor = _a[_i];
                            if (tileNeighbor.gameObject) {
                                if (this.excludeGameObjectsFromHitTestingInUpdateMoving
                                    && this.excludeGameObjectsFromHitTestingInUpdateMoving.indexOf(tileNeighbor.gameObject) >= 0) {
                                    continue;
                                }
                                if (this.hitTestWith(tileNeighbor.gameObject)) {
                                    this.position.set(prevPosX, prevPosY);
                                    return tileNeighbor.gameObject;
                                }
                            }
                        }
                        this.findClosestTile();
                    }
                    return null;
                };
                return MovableGameObject;
            }(object.AbstractGameObjectModel));
            object.MovableGameObject = MovableGameObject;
            var Direction;
            (function (Direction) {
                Direction[Direction["RIGHT"] = 0] = "RIGHT";
                Direction[Direction["LEFT"] = Math.PI] = "LEFT";
                Direction[Direction["UP"] = -Math.PI / 2] = "UP";
                Direction[Direction["DOWN"] = Math.PI / 2] = "DOWN";
            })(Direction = object.Direction || (object.Direction = {}));
            var allDirections = [Direction.RIGHT, Direction.LEFT, Direction.UP, Direction.DOWN];
            function getRandomDirection() {
                return CommonUtils.getRandomItem(allDirections);
            }
            object.getRandomDirection = getRandomDirection;
        })(object = model.object || (model.object = {}));
    })(model = src.model || (src.model = {}));
})(src || (src = {}));
///<reference path="../AbstractGameObjectModel.ts"/>
var src;
(function (src) {
    var model;
    (function (model) {
        var object;
        (function (object) {
            var bullet;
            (function (bullet_1) {
                var TankBulletModel = (function (_super) {
                    __extends(TankBulletModel, _super);
                    function TankBulletModel(tankModel, tile, position, axleSpeedModule, _direction, _damage) {
                        var _this = _super.call(this, axleSpeedModule, _direction) || this;
                        _this.excludeGameObjectsFromHitTestingInUpdateMoving = [tankModel];
                        _this.tile = tile;
                        _this.position.set(position.x, position.y);
                        _this._damage = _damage;
                        _this.isMoving = true;
                        return _this;
                    }
                    Object.defineProperty(TankBulletModel.prototype, "tile", {
                        set: function (_tile) {
                            this._tile = _tile;
                        },
                        enumerable: true,
                        configurable: true
                    });
                    Object.defineProperty(TankBulletModel.prototype, "damage", {
                        get: function () {
                            return this._damage;
                        },
                        enumerable: true,
                        configurable: true
                    });
                    TankBulletModel.prototype.hitByBullet = function (bullet) {
                        return false;
                    };
                    return TankBulletModel;
                }(object.MovableGameObject));
                bullet_1.TankBulletModel = TankBulletModel;
            })(bullet = object.bullet || (object.bullet = {}));
        })(object = model.object || (model.object = {}));
    })(model = src.model || (src.model = {}));
})(src || (src = {}));
///<reference path="../AbstractGameObjectModel.ts"/>
///<reference path="../../../util/CommonUtils.ts"/>
///<reference path="../MovableGameObject.ts"/>
///<reference path="../bullet/TankBulletModel.ts"/>
var src;
(function (src) {
    var model;
    (function (model) {
        var object;
        (function (object) {
            var tank;
            (function (tank) {
                var TankBulletModel = src.model.object.bullet.TankBulletModel;
                var TankModel = (function (_super) {
                    __extends(TankModel, _super);
                    function TankModel(_shootingStartBulletLocalRelativePositions, bulletDamage, axleSpeedModule, bulletAxleSpeedModule, _reloadingDurationInUpdates) {
                        if (axleSpeedModule === void 0) { axleSpeedModule = 1; }
                        if (bulletAxleSpeedModule === void 0) { bulletAxleSpeedModule = 3; }
                        if (_reloadingDurationInUpdates === void 0) { _reloadingDurationInUpdates = 60; }
                        var _this = _super.call(this, axleSpeedModule, object.Direction.RIGHT) || this;
                        _this._isShooting = false;
                        _this._currentReloadingUpdate = 0;
                        _this.bulletAxleSpeedModule = bulletAxleSpeedModule;
                        _this._shootingStartBulletLocalRelativePositions = _shootingStartBulletLocalRelativePositions;
                        _this.bulletDamage = bulletDamage;
                        _this._reloadingDurationInUpdates = _reloadingDurationInUpdates;
                        return _this;
                    }
                    Object.defineProperty(TankModel.prototype, "isShooting", {
                        get: function () {
                            return this._isShooting;
                        },
                        set: function (_isShooting) {
                            this._isShooting = _isShooting;
                        },
                        enumerable: true,
                        configurable: true
                    });
                    Object.defineProperty(TankModel.prototype, "viewBounds", {
                        get: function () {
                            return this._viewBounds;
                        },
                        set: function (_viewBounds) {
                            this._viewBounds = _viewBounds;
                            this.directionToShootingStartBulletLocalRelativePositionsMapArray = [];
                            for (var _i = 0, _a = this._shootingStartBulletLocalRelativePositions; _i < _a.length; _i++) {
                                var _p = _a[_i];
                                var _absoluteP = new PIXI.Point(_p.x * this._viewBounds.width, _p.y * this._viewBounds.height);
                                var _map = new Object();
                                _map[object.Direction.RIGHT] = new PIXI.Point(_absoluteP.x, _absoluteP.y);
                                _map[object.Direction.LEFT] = new PIXI.Point(-_absoluteP.x, -_absoluteP.y);
                                _map[object.Direction.DOWN] = new PIXI.Point(-_absoluteP.y, _absoluteP.x);
                                _map[object.Direction.UP] = new PIXI.Point(_absoluteP.y, -_absoluteP.x);
                                this.directionToShootingStartBulletLocalRelativePositionsMapArray.push(_map);
                            }
                        },
                        enumerable: true,
                        configurable: true
                    });
                    TankModel.prototype.updateShooting = function () {
                        var tankBulletModels = null;
                        if (this.isShooting) {
                            if (this._currentReloadingUpdate == 0) {
                                tankBulletModels = [];
                                for (var _i = 0, _a = this.directionToShootingStartBulletLocalRelativePositionsMapArray; _i < _a.length; _i++) {
                                    var _map = _a[_i];
                                    var bulletPostion = this.position.clone();
                                    bulletPostion.x += _map[this.direction].x;
                                    bulletPostion.y += _map[this.direction].y;
                                    tankBulletModels.push(new TankBulletModel(this, this._tile, bulletPostion, this.bulletAxleSpeedModule, this.direction, this.bulletDamage));
                                }
                            }
                            this._currentReloadingUpdate = (this._currentReloadingUpdate + 1) % this._reloadingDurationInUpdates;
                        }
                        else {
                            if (this._currentReloadingUpdate != 0) {
                                this._currentReloadingUpdate = (this._currentReloadingUpdate + 1) % this._reloadingDurationInUpdates;
                            }
                        }
                        return tankBulletModels;
                    };
                    TankModel.prototype.hitByBullet = function (bullet) {
                        return true;
                    };
                    return TankModel;
                }(object.MovableGameObject));
                tank.TankModel = TankModel;
            })(tank = object.tank || (object.tank = {}));
        })(object = model.object || (model.object = {}));
    })(model = src.model || (src.model = {}));
})(src || (src = {}));
///<reference path="../AbstractGameObjectModel.ts"/>
var src;
(function (src) {
    var model;
    (function (model) {
        var object;
        (function (object) {
            var block;
            (function (block) {
                var AbstractBlockModel = (function (_super) {
                    __extends(AbstractBlockModel, _super);
                    function AbstractBlockModel() {
                        return _super.call(this) || this;
                    }
                    return AbstractBlockModel;
                }(object.AbstractGameObjectModel));
                block.AbstractBlockModel = AbstractBlockModel;
            })(block = object.block || (object.block = {}));
        })(object = model.object || (model.object = {}));
    })(model = src.model || (src.model = {}));
})(src || (src = {}));
///<reference path="AbstractBlockModel.ts"/>
var src;
(function (src) {
    var model;
    (function (model) {
        var object;
        (function (object) {
            var block;
            (function (block) {
                var HayBlockModel = (function (_super) {
                    __extends(HayBlockModel, _super);
                    function HayBlockModel(fullHealth) {
                        if (fullHealth === void 0) { fullHealth = 100.0; }
                        var _this = _super.call(this) || this;
                        _this.health = _this.fullHealth = fullHealth;
                        return _this;
                    }
                    /**
                     * @returns {number} health state as a value [0..1]
                     */
                    HayBlockModel.prototype.getHealthState = function () {
                        return this.health / this.fullHealth;
                    };
                    HayBlockModel.prototype.hitByBullet = function (bullet) {
                        this.health -= bullet.damage;
                        if (this.health < 0)
                            this.health = 0;
                        return this.health == 0;
                    };
                    return HayBlockModel;
                }(block.AbstractBlockModel));
                block.HayBlockModel = HayBlockModel;
            })(block = object.block || (object.block = {}));
        })(object = model.object || (model.object = {}));
    })(model = src.model || (src.model = {}));
})(src || (src = {}));
var src;
(function (src) {
    var viewmodel;
    (function (viewmodel) {
        var object;
        (function (object) {
            var MovableGameObject = src.model.object.MovableGameObject;
            var HayBlockModel = src.model.object.block.HayBlockModel;
            var GameObjectViewPool = src.factory.pool.GameObjectViewPool;
            var TankModel = src.model.object.tank.TankModel;
            var GameObjectViewModel = (function () {
                function GameObjectViewModel(view, model) {
                    var _this = this;
                    this.synchronize = null;
                    this.view = view;
                    this.model = model;
                    if (this.model instanceof TankModel) {
                        this.synchronize = function () {
                            _this.view.position.set(_this.model.position.x, _this.model.position.y);
                            _this.view.rotation = _this.model.direction;
                        };
                    }
                    else if (this.model instanceof MovableGameObject) {
                        this.synchronize = function () {
                            _this.view.position.set(_this.model.position.x, _this.model.position.y);
                        };
                    }
                    else if (this.model instanceof HayBlockModel) {
                        this.synchronize = function () {
                            _this.view.alpha = _this.model.getHealthState();
                        };
                    }
                }
                /**
                 * Retrieves a GameObjcetView for its model from the singleton pool GameObjectViewPool
                 * and binds with he model
                 * @param gmeObjectModel
                 */
                GameObjectViewModel.retrieveGameObjectView = function (gameObjectModel) {
                    var gameObjectView = GameObjectViewPool.instance.getElement(GameObjectViewModel.GAME_OBJECTS_MODEL_PREFIX_VIEW_NAME_MAP[gameObjectModel.constructor['name']], true);
                    return gameObjectView;
                };
                GameObjectViewModel.bind = function (gameObjectView, gameObjectModel) {
                    gameObjectView.position.set(gameObjectModel.position.x, gameObjectModel.position.y);
                    gameObjectModel.viewBounds = gameObjectView.getBounds();
                    var gameObjectViewModel = new GameObjectViewModel(gameObjectView, gameObjectModel);
                    return gameObjectViewModel;
                };
                GameObjectViewModel.GAME_OBJECTS_MODEL_PREFIX_VIEW_NAME_MAP = {
                    'RedTankModel': '_REDTANK_',
                    'BlueTankModel': '_BLUETANK_',
                    'GreenTankModel': '_GREENTANK_',
                    'TankBulletModel': '_BULLET_',
                    'HayBlockModel': '_HAYBLOCK_',
                    'WallBlockModel': '_WALLBLOCK_'
                };
                return GameObjectViewModel;
            }());
            object.GameObjectViewModel = GameObjectViewModel;
        })(object = viewmodel.object || (viewmodel.object = {}));
    })(viewmodel = src.viewmodel || (src.viewmodel = {}));
})(src || (src = {}));
///<reference path="../../factory/pool/GameObjectViewPool.ts"/>
///<reference path="../../model/object/tank/TankModel.ts"/>
///<reference path="../../model/object/block/HayBlockModel.ts"/>
///<reference path="../../viewmodel/object/GameObjectViewModel.ts"/>
var src;
(function (src) {
    var controler;
    (function (controler) {
        var gamestate;
        (function (gamestate) {
            var Direction = src.model.object.Direction;
            var GameObjectViewModel = src.viewmodel.object.GameObjectViewModel;
            var BattleAreaController = (function () {
                function BattleAreaController(battleAreModel, battleAreaView, gameObjectViewModelsArray, app) {
                    var _this = this;
                    this.tankBulletShootHandler = function (tbm) {
                        var gameObjectView = GameObjectViewModel.retrieveGameObjectView(tbm);
                        var gameObjectViewModel = GameObjectViewModel.bind(gameObjectView, tbm);
                        _this.gameObjectsViewModel.push(gameObjectViewModel);
                        _this.battleAreaView.addGameObjectView(gameObjectViewModel.view);
                    };
                    this.gameObjectDestructionHandler = function (agom) {
                        _this.battleAreaView.removeGameObjectView(_this.removeViewModelBy(agom).view);
                    };
                    this.isPressed = {
                        'DIRECTION': {
                            'LEFT': false,
                            'RIGHT': false,
                            'UP': false,
                            'DOWN': false
                        },
                        'SHOOTING': false
                    };
                    this.keyDownHandler = function (e) {
                        switch (e.keyCode) {
                            case src.Config.KEYCODES['TANK_DIRECTION_LEFT']:
                                _this.isPressed['DIRECTION']['LEFT'] = true;
                                break;
                            case src.Config.KEYCODES['TANK_DIRECTION_RIGHT']:
                                _this.isPressed['DIRECTION']['RIGHT'] = true;
                                break;
                            case src.Config.KEYCODES['TANK_DIRECTION_UP']:
                                _this.isPressed['DIRECTION']['UP'] = true;
                                break;
                            case src.Config.KEYCODES['TANK_DIRECTION_DOWN']:
                                _this.isPressed['DIRECTION']['DOWN'] = true;
                                break;
                            case src.Config.KEYCODES['TANK_SHOOTING']:
                                _this.isPressed['SHOOTING'] = true;
                                break;
                        }
                    };
                    this.keyUpHandler = function (e) {
                        switch (e.keyCode) {
                            case src.Config.KEYCODES['TANK_DIRECTION_LEFT']:
                                _this.isPressed['DIRECTION']['LEFT'] = false;
                                break;
                            case src.Config.KEYCODES['TANK_DIRECTION_RIGHT']:
                                _this.isPressed['DIRECTION']['RIGHT'] = false;
                                break;
                            case src.Config.KEYCODES['TANK_DIRECTION_UP']:
                                _this.isPressed['DIRECTION']['UP'] = false;
                                break;
                            case src.Config.KEYCODES['TANK_DIRECTION_DOWN']:
                                _this.isPressed['DIRECTION']['DOWN'] = false;
                                break;
                            case src.Config.KEYCODES['TANK_SHOOTING']:
                                _this.isPressed['SHOOTING'] = false;
                                break;
                            case src.Config.KEYCODES['NEXT_PLAYER_TANK']:
                                _this.battleAreModel.nextPlayerTank();
                                break;
                        }
                    };
                    this.battleAreModel = battleAreModel;
                    this.battleAreaView = battleAreaView;
                    this.gameObjectsViewModel = gameObjectViewModelsArray;
                    this.app = app;
                }
                BattleAreaController.prototype.startBattle = function () {
                    this.battleAreaView.show();
                    for (var _i = 0, _a = this.gameObjectsViewModel; _i < _a.length; _i++) {
                        var viewModel = _a[_i];
                        this.battleAreaView.addGameObjectView(viewModel.view);
                    }
                    this.battleAreModel.onTankBulletShotCallback = this.tankBulletShootHandler;
                    this.battleAreModel.onGameObjectDestroyedCallback = this.gameObjectDestructionHandler;
                    window.addEventListener('keydown', this.keyDownHandler);
                    window.addEventListener('keyup', this.keyUpHandler);
                    this.app.ticker.add(this.updateController, this);
                };
                BattleAreaController.prototype.getViewModelBy = function (model) {
                    for (var _i = 0, _a = this.gameObjectsViewModel; _i < _a.length; _i++) {
                        var viewModel = _a[_i];
                        if (viewModel.model == model) {
                            return viewModel;
                        }
                    }
                    return null;
                };
                BattleAreaController.prototype.removeViewModelBy = function (model) {
                    var vm = this.getViewModelBy(model);
                    if (vm) {
                        this.gameObjectsViewModel.splice(this.gameObjectsViewModel.indexOf(vm), 1);
                    }
                    return vm;
                };
                BattleAreaController.prototype.stopBattle = function () {
                    this.battleAreaView.hide();
                    window.removeEventListener('keydown', this.keyDownHandler);
                    window.removeEventListener('keyup', this.keyUpHandler);
                    this.battleAreModel.onTankBulletShotCallback = null;
                    this.app.ticker.remove(this.updateController, this);
                };
                BattleAreaController.prototype.updateKeyboardInteractivityActions = function () {
                    var pressedDirectionKeysNum = 0;
                    var pressedKeyDirection = null;
                    for (var keyDirection in this.isPressed['DIRECTION']) {
                        if (this.isPressed['DIRECTION'][keyDirection]) {
                            pressedDirectionKeysNum++;
                            pressedKeyDirection = keyDirection;
                        }
                    }
                    if (pressedDirectionKeysNum == 1) {
                        this.battleAreModel.setPlayerTankDirection(Direction[pressedKeyDirection]);
                        this.battleAreModel.setPlayerTankMoving(true);
                    }
                    else {
                        this.battleAreModel.setPlayerTankMoving(false);
                    }
                    this.battleAreModel.setPlayerTankShooting(this.isPressed['SHOOTING']);
                };
                BattleAreaController.prototype.updateController = function () {
                    this.updateKeyboardInteractivityActions();
                    this.battleAreModel.updateModel();
                    for (var i = 0; i < this.gameObjectsViewModel.length; i++) {
                        if (this.gameObjectsViewModel[i].synchronize) {
                            this.gameObjectsViewModel[i].synchronize();
                        }
                    }
                    this.battleAreaView.updateView();
                };
                return BattleAreaController;
            }());
            gamestate.BattleAreaController = BattleAreaController;
        })(gamestate = controler.gamestate || (controler.gamestate = {}));
    })(controler = src.controler || (src.controler = {}));
})(src || (src = {}));
var src;
(function (src) {
    var controler;
    (function (controler) {
        var gamestate;
        (function (gamestate) {
            var PreloadController = (function () {
                function PreloadController(preloadView) {
                    this.preloadView = preloadView;
                }
                PreloadController.prototype.startPreload = function () {
                    var _this = this;
                    this.preloadView.show();
                    PIXI.loader
                        .add(src.TEXTURE_NAME_CONFIG.GAME_TEXTURE)
                        .on('progress', function (loader, resources) {
                        _this.preloadView.updateLoadingProgress(loader.progress);
                    }, this)
                        .load(function (loader, resources) {
                        _this.preloadView.hide();
                        src.factory.gamestate.GameStatesFactory.instance.createBattleAreaController().startBattle();
                    });
                };
                return PreloadController;
            }());
            gamestate.PreloadController = PreloadController;
        })(gamestate = controler.gamestate || (controler.gamestate = {}));
    })(controler = src.controler || (src.controler = {}));
})(src || (src = {}));
///<reference path="../../util/CommonUtils.ts"/>
var src;
(function (src) {
    var model;
    (function (model) {
        var object;
        (function (object) {
            var TileModel = (function () {
                function TileModel(iMatrixPosition, jMatrixPosition, size) {
                    this._position = new PIXI.Point(iMatrixPosition * size, jMatrixPosition * size);
                }
                Object.defineProperty(TileModel.prototype, "neighborsAsDirectionMap", {
                    get: function () {
                        return this._neighborsAsDirectionMap;
                    },
                    set: function (_neighborsAsDirectionMap) {
                        this._neighborsAsDirectionMap = _neighborsAsDirectionMap;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(TileModel.prototype, "neighborsAsVector", {
                    get: function () {
                        return this._neighborsAsVector;
                    },
                    set: function (_neighbors) {
                        this._neighborsAsVector = _neighbors;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(TileModel.prototype, "gameObject", {
                    get: function () {
                        return this._gameObject;
                    },
                    set: function (_gameObject) {
                        this._gameObject = _gameObject;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(TileModel.prototype, "position", {
                    get: function () {
                        return this._position;
                    },
                    enumerable: true,
                    configurable: true
                });
                return TileModel;
            }());
            object.TileModel = TileModel;
        })(object = model.object || (model.object = {}));
    })(model = src.model || (src.model = {}));
})(src || (src = {}));
///<reference path="AbstractBlockModel.ts"/>
var src;
(function (src) {
    var model;
    (function (model) {
        var object;
        (function (object) {
            var block;
            (function (block) {
                var WallBlockModel = (function (_super) {
                    __extends(WallBlockModel, _super);
                    function WallBlockModel() {
                        return _super.call(this) || this;
                    }
                    WallBlockModel.prototype.hitByBullet = function (bullet) {
                        return false;
                    };
                    return WallBlockModel;
                }(block.AbstractBlockModel));
                block.WallBlockModel = WallBlockModel;
            })(block = object.block || (object.block = {}));
        })(object = model.object || (model.object = {}));
    })(model = src.model || (src.model = {}));
})(src || (src = {}));
///<reference path="TankModel.ts"/>
var src;
(function (src) {
    var model;
    (function (model) {
        var object;
        (function (object) {
            var tank;
            (function (tank) {
                var RedTankModel = (function (_super) {
                    __extends(RedTankModel, _super);
                    function RedTankModel() {
                        return _super.call(this, [
                            new PIXI.Point(0.5, -0.15),
                            new PIXI.Point(0.5, 0.15)
                        ], src.Config.RED_TANK_BULLET_DAMAGE, 1.5, 5, 50) || this;
                    }
                    return RedTankModel;
                }(tank.TankModel));
                tank.RedTankModel = RedTankModel;
            })(tank = object.tank || (object.tank = {}));
        })(object = model.object || (model.object = {}));
    })(model = src.model || (src.model = {}));
})(src || (src = {}));
///<reference path="TankModel.ts"/>
var src;
(function (src) {
    var model;
    (function (model) {
        var object;
        (function (object) {
            var tank;
            (function (tank) {
                var GreenTankModel = (function (_super) {
                    __extends(GreenTankModel, _super);
                    function GreenTankModel() {
                        return _super.call(this, [
                            new PIXI.Point(0.5, 0)
                        ], src.Config.GREEN_TANK_BULLET_DAMAGE, 2, 4.5, 40) || this;
                    }
                    return GreenTankModel;
                }(tank.TankModel));
                tank.GreenTankModel = GreenTankModel;
            })(tank = object.tank || (object.tank = {}));
        })(object = model.object || (model.object = {}));
    })(model = src.model || (src.model = {}));
})(src || (src = {}));
var src;
(function (src) {
    var model;
    (function (model) {
        var object;
        (function (object) {
            var tank;
            (function (tank) {
                var BlueTankModel = (function (_super) {
                    __extends(BlueTankModel, _super);
                    function BlueTankModel() {
                        return _super.call(this, [
                            new PIXI.Point(0.5, -0.25),
                            new PIXI.Point(0.5, 0),
                            new PIXI.Point(0.5, 0.25),
                        ], src.Config.BLUE_TANK_BULLET_DAMAGE, 1, 3.5, 60) || this;
                    }
                    return BlueTankModel;
                }(tank.TankModel));
                tank.BlueTankModel = BlueTankModel;
            })(tank = object.tank || (object.tank = {}));
        })(object = model.object || (model.object = {}));
    })(model = src.model || (src.model = {}));
})(src || (src = {}));
///<reference path="../../model/object/TileModel.ts"/>
var src;
(function (src) {
    var factory;
    (function (factory) {
        var object;
        (function (object) {
            var TileModel = src.model.object.TileModel;
            var Direction = src.model.object.Direction;
            var AbstractTilesMatrixFactory = (function () {
                /**
                 * Creates free matrix
                 * @returns {Array<Array<src.model.object.TileModel>>}
                 */
                function AbstractTilesMatrixFactory() {
                    this.matrix = [];
                    this.tankModels = [];
                    for (var i = 0; i < src.Config.MAP_TILES_HORIZONTAL_NUM; i++) {
                        var array = [];
                        for (var j = 0; j < src.Config.MAP_TILES_VERTICAL_NUM; j++) {
                            var tile = new TileModel(i, j, src.Config.TILE_SIZE_PX);
                            array.push(tile);
                        }
                        this.matrix.push(array);
                    }
                    //setup neighborsAsVector
                    for (var i = 0; i < src.Config.MAP_TILES_HORIZONTAL_NUM; i++) {
                        for (var j = 0; j < src.Config.MAP_TILES_VERTICAL_NUM; j++) {
                            var tile = this.matrix[i][j];
                            var neighborsAsVector = [];
                            var neighborsAsDirectionMap = new Object();
                            for (var k = -1; k <= 1; k++) {
                                for (var l = -1; l <= 1; l++) {
                                    if (k == 0 && l == 0)
                                        continue;
                                    var _i = i + k;
                                    var _j = j + l;
                                    if (_i >= 0 && _i < src.Config.MAP_TILES_HORIZONTAL_NUM &&
                                        _j >= 0 && _j < src.Config.MAP_TILES_VERTICAL_NUM) {
                                        neighborsAsVector.push(this.matrix[_i][_j]);
                                        var _direction = null;
                                        if (k == 1 && l == 0) {
                                            _direction = Direction.RIGHT;
                                        }
                                        else if (k == 0 && l == 1) {
                                            _direction = Direction.DOWN;
                                        }
                                        else if (k == -1 && l == 0) {
                                            _direction = Direction.LEFT;
                                        }
                                        else if (k == 0 && l == -1) {
                                            _direction = Direction.UP;
                                        }
                                        if (_direction != null) {
                                            neighborsAsDirectionMap[_direction] = this.matrix[_i][_j];
                                        }
                                    }
                                }
                            }
                            tile.neighborsAsVector = neighborsAsVector;
                            tile.neighborsAsDirectionMap = neighborsAsDirectionMap;
                        }
                    }
                }
                AbstractTilesMatrixFactory.prototype.getFreeMatrixTilesAsArray = function () {
                    var freeTiles = [];
                    for (var i = 0; i < this.matrix.length; i++) {
                        for (var j = 0; j < this.matrix[i].length; j++) {
                            if (this.matrix[i][j].gameObject == null) {
                                freeTiles.push(this.matrix[i][j]);
                            }
                        }
                    }
                    return freeTiles;
                };
                return AbstractTilesMatrixFactory;
            }());
            object.AbstractTilesMatrixFactory = AbstractTilesMatrixFactory;
        })(object = factory.object || (factory.object = {}));
    })(factory = src.factory || (src.factory = {}));
})(src || (src = {}));
///<reference path="../../model/object/TileModel.ts"/>
///<reference path="../../model/object/block/HayBlockModel.ts"/>
///<reference path="../../model/object/block/WallBlockModel.ts"/>
///<reference path="../../model/object/tank/RedTankModel.ts"/>
///<reference path="../../model/object/tank/GreenTankModel.ts"/>
///<reference path="../../model/object/tank/BlueTankModel.ts"/>
///<reference path="../../util/CommonUtils.ts"/>
///<reference path="AbstractTilesMatrixFactory.ts"/>
var src;
(function (src) {
    var factory;
    (function (factory) {
        var object;
        (function (object) {
            var HayBlockModel = src.model.object.block.HayBlockModel;
            var WallBlockModel = src.model.object.block.WallBlockModel;
            var RedTankModel = src.model.object.tank.RedTankModel;
            var GreenTankModel = src.model.object.tank.GreenTankModel;
            var BlueTankModel = src.model.object.tank.BlueTankModel;
            var CommonUtils = src.util.CommonUtils;
            var RandomTilesMatrixFactory = (function (_super) {
                __extends(RandomTilesMatrixFactory, _super);
                /**
                 * Creates and randomly fills matrix
                 * @returns {Array<Array<src.model.object.TileModel>>}
                 */
                function RandomTilesMatrixFactory() {
                    var _this = _super.call(this) || this;
                    _this.matrixFillingByBlocks = 0.4;
                    _this.hayBlockModelProbability = 0.75;
                    var freeTiles = _this.getFreeMatrixTilesAsArray();
                    _this.generateTankModels();
                    _this.fill(freeTiles, _this.tankModels);
                    var blocksCount = Math.floor(freeTiles.length * _this.matrixFillingByBlocks);
                    var blocksModels = _this.generateBlockObjectModels(blocksCount, _this.hayBlockModelProbability);
                    _this.fill(freeTiles, blocksModels);
                    return _this;
                }
                /**
                 * Generates three different tanks
                 * @returns {Array<src.model.object.tank.AbstractTankModel>}
                 */
                RandomTilesMatrixFactory.prototype.generateTankModels = function () {
                    this.tankModels.push(new RedTankModel(), new GreenTankModel(), new BlueTankModel());
                };
                RandomTilesMatrixFactory.prototype.generateBlockObjectModels = function (count, hayBlockModelProbability) {
                    var rndArray = new Array(count);
                    for (var i = 0; i < count; i++) {
                        rndArray[i] = Math.random() < hayBlockModelProbability ? new HayBlockModel() : new WallBlockModel();
                    }
                    return rndArray;
                };
                RandomTilesMatrixFactory.prototype.fill = function (freeTiles, gameObjectModels) {
                    for (var i = 0; i < gameObjectModels.length && freeTiles.length > 0; i++) {
                        var rndFreeTileModel = CommonUtils.removeRandomItem(freeTiles, 0);
                        gameObjectModels[i].tile = rndFreeTileModel;
                        gameObjectModels[i].position.set(rndFreeTileModel.position.x, rndFreeTileModel.position.y);
                    }
                };
                return RandomTilesMatrixFactory;
            }(object.AbstractTilesMatrixFactory));
            object.RandomTilesMatrixFactory = RandomTilesMatrixFactory;
        })(object = factory.object || (factory.object = {}));
    })(factory = src.factory || (src.factory = {}));
})(src || (src = {}));
///<reference path="../object/RandomTilesMatrixFactory.ts"/>
///<reference path="../../viewmodel/object/GameObjectViewModel.ts"/>
var src;
(function (src) {
    var factory;
    (function (factory) {
        var gamestate;
        (function (gamestate) {
            var RandomTilesMatrixFactory = src.factory.object.RandomTilesMatrixFactory;
            var GameObjectViewModel = src.viewmodel.object.GameObjectViewModel;
            var GameStatesFactory = (function () {
                function GameStatesFactory(app) {
                    this.app = app;
                    GameStatesFactory.instance = this;
                }
                //***************************************** Preload MVC ******************************************/
                GameStatesFactory.prototype.createPreloadView = function () {
                    return new src.view.gamestate.PreloadView(this.app);
                };
                GameStatesFactory.prototype.createPreloadController = function () {
                    return new src.controler.gamestate.PreloadController(GameStatesFactory.instance.createPreloadView());
                };
                //***************************************** BattleAre MVC ******************************************/
                GameStatesFactory.prototype.createBattleAreaModel = function () {
                    var rndtmf = new RandomTilesMatrixFactory();
                    return new src.model.gamestate.BattleAreaModel(rndtmf.matrix, rndtmf.tankModels);
                };
                GameStatesFactory.prototype.createBattleAreaController = function () {
                    var battleAreModel = GameStatesFactory.instance.createBattleAreaModel();
                    var gameObjectViewModelsArray = [];
                    for (var _i = 0, _a = battleAreModel.tilesMatrix; _i < _a.length; _i++) {
                        var row = _a[_i];
                        for (var _b = 0, row_1 = row; _b < row_1.length; _b++) {
                            var tile = row_1[_b];
                            if (tile.gameObject) {
                                var gameObjectView = GameObjectViewModel.retrieveGameObjectView(tile.gameObject);
                                var gameObjectViewModel = GameObjectViewModel.bind(gameObjectView, tile.gameObject);
                                gameObjectViewModelsArray.push(gameObjectViewModel);
                            }
                        }
                    }
                    return new src.controler.gamestate.BattleAreaController(battleAreModel, GameStatesFactory.instance.createBattleAreaView(), gameObjectViewModelsArray, this.app);
                };
                GameStatesFactory.prototype.createBattleAreaView = function () {
                    return new src.view.gamestate.BattleAreaView(this.app);
                };
                return GameStatesFactory;
            }());
            gamestate.GameStatesFactory = GameStatesFactory;
        })(gamestate = factory.gamestate || (factory.gamestate = {}));
    })(factory = src.factory || (src.factory = {}));
})(src || (src = {}));
var src;
(function (src) {
    var model;
    (function (model) {
        var gamestate;
        (function (gamestate) {
            var TankModel = src.model.object.tank.TankModel;
            var CommonUtils = src.util.CommonUtils;
            var getRandomDirection = src.model.object.getRandomDirection;
            var BattleAreaModel = (function () {
                function BattleAreaModel(tilesMatrix, tanks) {
                    this._playerTank = null;
                    this._tankBullets = [];
                    this._onTankBulletShotCallback = null;
                    this._onGameObjectDestroyedCallback = null;
                    this._tilesMatrix = tilesMatrix;
                    this._tanks = tanks;
                    this.nextPlayerTank();
                    this._tanks[0].isShooting = true;
                    this._tanks[1].isShooting = true;
                }
                Object.defineProperty(BattleAreaModel.prototype, "tilesMatrix", {
                    get: function () {
                        return this._tilesMatrix;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(BattleAreaModel.prototype, "onTankBulletShotCallback", {
                    set: function (_onTankBulletShotCallback) {
                        this._onTankBulletShotCallback = _onTankBulletShotCallback;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(BattleAreaModel.prototype, "onGameObjectDestroyedCallback", {
                    set: function (_onGameObjectDestroyedCallback) {
                        this._onGameObjectDestroyedCallback = _onGameObjectDestroyedCallback;
                    },
                    enumerable: true,
                    configurable: true
                });
                BattleAreaModel.prototype.nextPlayerTank = function () {
                    if (this._playerTank) {
                        if (this._tanks.length < 2) {
                            //It doesn't make any sense to change the same tank
                            return;
                        }
                        this.setPlayerTankMoving(false);
                        this.setPlayerTankShooting(false);
                    }
                    this._playerTank = this._tanks.shift();
                    this._tanks.push(this._playerTank);
                };
                BattleAreaModel.prototype.setPlayerTankDirection = function (direction) {
                    this._playerTank.direction = direction;
                };
                BattleAreaModel.prototype.setPlayerTankMoving = function (isMoving) {
                    this._playerTank.isMoving = isMoving;
                };
                BattleAreaModel.prototype.setPlayerTankShooting = function (isShooting) {
                    this._playerTank.isShooting = isShooting;
                };
                BattleAreaModel.prototype.updateModel = function () {
                    this.updateSimpleRandomBasedAI();
                    for (var _i = 0, _a = this._tanks; _i < _a.length; _i++) {
                        var _tank = _a[_i];
                        _tank.updateMoving();
                        var tankBulletModels = _tank.updateShooting();
                        if (tankBulletModels) {
                            this._tankBullets = this._tankBullets.concat(tankBulletModels);
                            for (var _b = 0, tankBulletModels_1 = tankBulletModels; _b < tankBulletModels_1.length; _b++) {
                                var tbm = tankBulletModels_1[_b];
                                if (this._onTankBulletShotCallback)
                                    this._onTankBulletShotCallback(tbm);
                            }
                        }
                    }
                    for (var i = this._tankBullets.length - 1; i >= 0; i--) {
                        var intersectedWith = this._tankBullets[i].updateMoving();
                        if (intersectedWith) {
                            var destroyed = intersectedWith.hitByBullet(this._tankBullets[i]);
                            if (destroyed) {
                                if (intersectedWith instanceof TankModel) {
                                    var rndFreeTile = this.getRandomFreeTile();
                                    if (rndFreeTile) {
                                        intersectedWith.tile = rndFreeTile;
                                        intersectedWith.position.set(rndFreeTile.position.x, rndFreeTile.position.y);
                                    }
                                    else {
                                        alert('NO FREE TILES! RELOAD PAGE!');
                                    }
                                }
                                else {
                                    intersectedWith.tile = null;
                                    this._onGameObjectDestroyedCallback(intersectedWith);
                                }
                            }
                            this._onGameObjectDestroyedCallback(this._tankBullets[i]);
                            this._tankBullets[i].tile = null;
                            this._tankBullets.splice(i, 1);
                        }
                    }
                };
                BattleAreaModel.prototype.getRandomFreeTile = function () {
                    var freeTiles = [];
                    for (var _i = 0, _a = this.tilesMatrix; _i < _a.length; _i++) {
                        var _row = _a[_i];
                        for (var _b = 0, _row_1 = _row; _b < _row_1.length; _b++) {
                            var _tile = _row_1[_b];
                            if (_tile.gameObject == null) {
                                freeTiles.push(_tile);
                            }
                        }
                    }
                    return CommonUtils.getRandomItem(freeTiles);
                };
                //------------------SIMPLE AI IMITATION-----------------------//
                BattleAreaModel.prototype.updateSimpleRandomBasedAI = function () {
                    for (var _i = 0, _a = this._tanks; _i < _a.length; _i++) {
                        var _tank = _a[_i];
                        if (_tank != this._playerTank) {
                            if (_tank.isMoving) {
                                if (Math.random() > 0.99) {
                                    _tank.isMoving = false;
                                }
                            }
                            else {
                                if (Math.random() > 0.1) {
                                    _tank.isMoving = true;
                                }
                            }
                            if (Math.random() > 0.99) {
                                _tank.direction = getRandomDirection();
                            }
                            if (_tank.isShooting) {
                                if (Math.random() > 0.98) {
                                    _tank.isShooting = false;
                                }
                            }
                            else {
                                if (Math.random() > 0.85) {
                                    _tank.isShooting = true;
                                }
                            }
                        }
                    }
                };
                return BattleAreaModel;
            }());
            gamestate.BattleAreaModel = BattleAreaModel;
        })(gamestate = model.gamestate || (model.gamestate = {}));
    })(model = src.model || (src.model = {}));
})(src || (src = {}));
var src;
(function (src) {
    var view;
    (function (view) {
        var gamestate;
        (function (gamestate) {
            var AbstractGameStateView = (function (_super) {
                __extends(AbstractGameStateView, _super);
                function AbstractGameStateView(app) {
                    var _this = _super.call(this) || this;
                    _this.app = app;
                    return _this;
                }
                return AbstractGameStateView;
            }(PIXI.Container));
            gamestate.AbstractGameStateView = AbstractGameStateView;
        })(gamestate = view.gamestate || (view.gamestate = {}));
    })(view = src.view || (src.view = {}));
})(src || (src = {}));
var src;
(function (src) {
    var view;
    (function (view) {
        var gamestate;
        (function (gamestate) {
            var BattleAreaView = (function (_super) {
                __extends(BattleAreaView, _super);
                function BattleAreaView(app) {
                    return _super.call(this, app) || this;
                }
                BattleAreaView.prototype.show = function () {
                    this.app.stage.addChild(this);
                    //UI control hint
                    var controlHint = new PIXI.Text('[ARROW KEYS] - MOVE\n' +
                        '[SPACE] - SHOOTING\n' +
                        '[C] - CHANGE TANK\n', {
                        fontFamily: src.Config.FONT,
                        fontSize: 25,
                        fill: 0x000000,
                        align: 'center'
                    });
                    controlHint.position.set(100, 50);
                    controlHint.anchor.set(0.5, 0.5);
                    this.addChild(controlHint);
                };
                BattleAreaView.prototype.hide = function () {
                    this.parent.removeChild(this);
                };
                BattleAreaView.prototype.addGameObjectView = function (gameObjcetView) {
                    this.addChildAt(gameObjcetView, this.children.length - 1 /*because of UI control hint*/);
                };
                BattleAreaView.prototype.removeGameObjectView = function (gameObjcetView) {
                    this.removeChild(gameObjcetView);
                };
                BattleAreaView.prototype.updateView = function () {
                };
                return BattleAreaView;
            }(gamestate.AbstractGameStateView));
            gamestate.BattleAreaView = BattleAreaView;
        })(gamestate = view.gamestate || (view.gamestate = {}));
    })(view = src.view || (src.view = {}));
})(src || (src = {}));
var src;
(function (src) {
    var view;
    (function (view) {
        var gamestate;
        (function (gamestate) {
            var PreloadView = (function (_super) {
                __extends(PreloadView, _super);
                function PreloadView(app) {
                    return _super.call(this, app) || this;
                }
                PreloadView.prototype.show = function () {
                    this.indicator = new PIXI.Text('0%', {
                        fontFamily: src.Config.FONT,
                        fontSize: 30,
                        fill: 0x000000,
                        align: 'center'
                    });
                    this.indicator.position.set(this.app.renderer.width / 2, this.app.renderer.height / 2);
                    this.indicator.anchor.set(0.5, 0.5);
                    this.addChild(this.indicator);
                    this.app.stage.addChild(this);
                };
                PreloadView.prototype.hide = function () {
                    this.parent.removeChild(this);
                };
                /**
                 *
                 * @param {number} progress, values 0..100
                 */
                PreloadView.prototype.updateLoadingProgress = function (progress) {
                    this.indicator.text = progress + '%';
                };
                return PreloadView;
            }(gamestate.AbstractGameStateView));
            gamestate.PreloadView = PreloadView;
        })(gamestate = view.gamestate || (view.gamestate = {}));
    })(view = src.view || (src.view = {}));
})(src || (src = {}));
var src;
(function (src) {
    var view;
    (function (view) {
        var object;
        (function (object) {
            var Sprite = PIXI.Sprite;
            var CommonUtils = src.util.CommonUtils;
            var GameObjectView = (function (_super) {
                __extends(GameObjectView, _super);
                function GameObjectView(prefix) {
                    var _this = _super.call(this, CommonUtils.getSingleFrameTextrure(src.TEXTURE_NAME_CONFIG.GAME_TEXTURE, prefix)) || this;
                    _this._prefix = prefix;
                    _this.anchor.set(0.5, 0.5);
                    return _this;
                }
                Object.defineProperty(GameObjectView.prototype, "prefix", {
                    get: function () {
                        return this._prefix;
                    },
                    enumerable: true,
                    configurable: true
                });
                return GameObjectView;
            }(Sprite));
            object.GameObjectView = GameObjectView;
        })(object = view.object || (view.object = {}));
    })(view = src.view || (src.view = {}));
})(src || (src = {}));
//# sourceMappingURL=game.js.map